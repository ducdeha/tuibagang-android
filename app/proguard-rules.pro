# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\AndroidStudio\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn okio.**
-keepattributes Signature
-keep class com.facebook.model.** { *; }

-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
-keep class com.facebook.** { *; }

-keep class com.squareup.okhttp.** { *; }
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.** { *; }

-keep class com.kiemtien.tuibagang.login.ObjLogin { *; }

# Kiem Tien
-keep class com.kiemtien.tuibagang.subscreens.kiemtien.ObjKiemTien { *; }
-keep class com.kiemtien.tuibagang.subscreens.kiemtien.ObjKiemTien$ArrAppBean{ *; }
-keep class com.kiemtien.tuibagang.subscreens.kiemtien.ObjKiemTien$ArrAppBean$DescBean{ *; }

#Events
-keep class com.kiemtien.tuibagang.subscreens.events.ObjEvents { *; }
-keep class com.kiemtien.tuibagang.subscreens.events.ObjEvents$ArrEventsBean{ *; }

#DoiThuong
-keep class com.kiemtien.tuibagang.subscreens.doithuong.ObjDoiThuong { *; }
-keep class com.kiemtien.tuibagang.subscreens.doithuong.ObjDoiThuong$ArrCardsBean$ProductInfo{ *; }
-keep class com.kiemtien.tuibagang.subscreens.doithuong.ObjDoiThuong$ArrCardsBean{ *; }

#CauHoiThuongGap
-keep class com.kiemtien.tuibagang.subscreens.canhan.cauhoithuonggap.ObjCauHoiThuongGap { *; }

#Lien He
-keep class com.kiemtien.tuibagang.subscreens.canhan.lienhe.ObjLienHe { *; }

#LsHoatDong
-keep class com.kiemtien.tuibagang.subscreens.canhan.lshoatdong.ObjLsHoatDong { *; }

#Lich su nhan thuong
-keep class com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong.ObjLsNhanThuong { *; }
-keep class com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong.ObjLsNhanThuong$CardBean { *; }

#Top kiem tien
-keep class com.kiemtien.tuibagang.subscreens.canhan.topkiemtien.ObjTopRank { *; }



