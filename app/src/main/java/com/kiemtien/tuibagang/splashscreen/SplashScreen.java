package com.kiemtien.tuibagang.splashscreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.login.LoginActivity;
import com.kiemtien.tuibagang.login.ObjLogin;

import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.io.IOException;

/**
 * Created by DucNT on 4/20/16.
 */
public class SplashScreen extends Activity {
    private Thread mSplashThread;
    public static final String SENDER_ID = "225830668556";
    private GoogleCloudMessaging gcm;
    private String regid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash_activity);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.splash);
        gcm = GoogleCloudMessaging.getInstance(this);
        regid = SharePreferences.getDeviceTokenPreference(this, SharePreferences.KEY_PREF_REGISTER_GCM);
        startAnimations(linearLayout);
        if (regid == null || regid.isEmpty() || isVersionAppChange()) {
            registerInBackground();
        }
    }

    private void startAnimations(LinearLayout linearLayout) {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.blink);
        anim.reset();
        linearLayout.clearAnimation();
        linearLayout.startAnimation(anim);
        mSplashThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Thời gian này giống thời gian set ở trong R.anim.blink
                    while (waited < 2000) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = null;
                    ObjLogin objLogin = SharePreferences.getUserInfo(SplashScreen.this);
                    if (objLogin != null) intent = new Intent(SplashScreen.this,
                            MainActivity.class);
                    else
                        intent = new Intent(SplashScreen.this,
                                LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    SplashScreen.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }

            }
        };
        mSplashThread.start();
    }


    @SuppressLint("NewApi")
    private void registerInBackground() {
        new AsyncTask<Void, String, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID = " + regid;
                    storeRegistrationId(regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId(String regId) {
        SharePreferences.saveDeviceTokenPreference(this,
                SharePreferences.KEY_PREF_REGISTER_GCM, regId);
        int appVersion = CommonFunc.getAppVersion(this);
        SharePreferences.saveAppVersionPreference(this,
                SharePreferences.KEY_PREF_APP_VERSION, appVersion);

    }

    private boolean isVersionAppChange() {
        int oldVersion = SharePreferences.getAppVersionPreference(this, SharePreferences.KEY_PREF_APP_VERSION);
        int newVersion = CommonFunc.getAppVersion(this);
        if (oldVersion != -1 && oldVersion != newVersion) {
            return true;
        }
        return false;
    }
}
