package com.kiemtien.tuibagang.subscreens.doithuong;

import java.util.List;

/**
 * Created by DucNT on 4/22/16.
 */
public class ObjDoiThuong {


    /**
     * returnVal : 1
     * arr_cards : {"Viettel":[{"mobile_card_id":"1","mobile_card_price":"10000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"},{"mobile_card_id":"2","mobile_card_price":"20000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"},{"mobile_card_id":"3","mobile_card_price":"50000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"},{"mobile_card_id":"4","mobile_card_price":"100000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"},{"mobile_card_id":"5","mobile_card_price":"200000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"},{"mobile_card_id":"6","mobile_card_price":"500000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"Viettel"}],"MobiFone":[{"mobile_card_id":"7","mobile_card_price":"10000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"},{"mobile_card_id":"8","mobile_card_price":"20000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"},{"mobile_card_id":"9","mobile_card_price":"50000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"},{"mobile_card_id":"10","mobile_card_price":"100000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"},{"mobile_card_id":"11","mobile_card_price":"200000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"},{"mobile_card_id":"12","mobile_card_price":"500000","mobile_card_img":"https://lh3.googleusercontent.com/D1sMqMf8tqws2zOg3HQUCvFksssv9NcZ4ctpHYEgAlkGSscdyZCpwXpT6CUw8il4BA=w300-rw","mobile_card_status":"0","mobile_card_type_name":"MobiFone"}]}
     */

    private int returnVal;
    private String coin;

    private ArrCardsBean arr_cards;

    public int getReturnVal() {
        return returnVal;
    }

    public void setReturnVal(int returnVal) {
        this.returnVal = returnVal;
    }

    public ArrCardsBean getArr_cards() {
        return arr_cards;
    }

    public void setArr_cards(ArrCardsBean arr_cards) {
        this.arr_cards = arr_cards;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public static class ArrCardsBean {

        private List<ProductInfo> Viettel;

        private List<ProductInfo> MobiFone;
        private List<ProductInfo> Vinaphone;
        private List<ProductInfo> Vietnamobile;
        private List<ProductInfo> Gmobile;
        private List<ProductInfo> SFone;

        public List<ProductInfo> getVinaphone() {
            return  Vinaphone;
        }

        public void setVinaphone(List<ProductInfo> vinaphone) {
            Vinaphone = vinaphone;
        }

        public List<ProductInfo> getVietnamobile() {
            return Vietnamobile;
        }

        public void setVietnamobile(List<ProductInfo> vietnamobile) {
            Vietnamobile = vietnamobile;
        }

        public List<ProductInfo> getGmobile() {
            return Gmobile;
        }

        public void setGmobile(List<ProductInfo> gmobile) {
            Gmobile = gmobile;
        }

        public List<ProductInfo> getSFone() {
            return SFone;
        }

        public void setSFone(List<ProductInfo> SFone) {
            this.SFone = SFone;
        }

        public List<ProductInfo> getViettel() {
            return Viettel;
        }

        public void setViettel(List<ProductInfo> Viettel) {
            this.Viettel = Viettel;
        }

        public List<ProductInfo> getMobiFone() {
            return MobiFone;
        }

        public void setMobiFone(List<ProductInfo> MobiFone) {
            this.MobiFone = MobiFone;
        }


        public static class ProductInfo {
            private String mobile_card_id;
            private String mobile_card_price;
            private String mobile_card_img;
            private String mobile_card_status;
            private String mobile_card_type_name;

            public String getMobile_card_id() {
                return mobile_card_id;
            }

            public void setMobile_card_id(String mobile_card_id) {
                this.mobile_card_id = mobile_card_id;
            }

            public String getMobile_card_price() {
                return mobile_card_price;
            }

            public void setMobile_card_price(String mobile_card_price) {
                this.mobile_card_price = mobile_card_price;
            }

            public String getMobile_card_img() {
                return mobile_card_img;
            }

            public void setMobile_card_img(String mobile_card_img) {
                this.mobile_card_img = mobile_card_img;
            }

            public String getMobile_card_status() {
                return mobile_card_status;
            }

            public void setMobile_card_status(String mobile_card_status) {
                this.mobile_card_status = mobile_card_status;
            }

            public String getMobile_card_type_name() {
                return mobile_card_type_name;
            }

            public void setMobile_card_type_name(String mobile_card_type_name) {
                this.mobile_card_type_name = mobile_card_type_name;
            }
        }
    }
}
