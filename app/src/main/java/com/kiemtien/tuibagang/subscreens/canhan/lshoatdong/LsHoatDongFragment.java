package com.kiemtien.tuibagang.subscreens.canhan.lshoatdong;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.network.ApiUrl;

/**
 * Created by DucNT on 4/23/16.
 */
public class LsHoatDongFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private LsHoatDongAdapter mLsHoatDongAdapter;
    private ArrayList<ObjLsHoatDong> mArrData = new ArrayList<>();
    private ObjLogin mObjLogin;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
        ((MainActivity) mActivity).setToolbarTitle(getResources().getString(R.string.ls_hoatdong));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) mActivity).setToolbarTitle("");
    }

    @Override
    public int getLayout() {
        return R.layout.kiemtien_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.content_view);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mLsHoatDongAdapter = new LsHoatDongAdapter(mActivity,mArrData,true);
        mListView.setAdapter(mLsHoatDongAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
    }


    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mListView.setEnabled(false);
                if(direction== SwipyRefreshLayoutDirection.TOP){
                    mArrData.clear();
                    loadData(false);
                }else {
                    loadData(true);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.OFFSET, "" + mArrData.size());
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_ACTIVITY;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                Gson gson = new Gson();
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray arrHistory = object.getJSONArray("arr_activitys");
                    for (int i = 0 ; i < arrHistory.length();i++) {
                        ObjLsHoatDong objLsHoatDong = gson.fromJson(arrHistory.get(i).toString(),ObjLsHoatDong.class);
                        mArrData.add(objLsHoatDong);
                    }
                    mLsHoatDongAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }
}

