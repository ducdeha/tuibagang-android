package com.kiemtien.tuibagang.subscreens.canhan.lshoatdong;

/**
 * Created by DucNT on 4/25/16.
 */
public class ObjLsHoatDong {

    /**
     * activity_id : 4
     * date : 01/01/1970
     * content : Cài đặt ứng dụng Despicable Me
     * coin : 150
     */

    private String activity_id;
    private String date;
    private String content;
    private String coin;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }
}
