package com.kiemtien.tuibagang.subscreens.doithuong;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoadingRec;

/**
 * Created by DucNT on 4/20/16.
 */
public class DoiThuongAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private int mType ;

    public DoiThuongAdapter(Context context, ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> arrApps,int type) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
        mType = type;
    }

    @Override
    public int getCount() {
        if(mArrData==null) mArrData = new ArrayList<>();
        return mArrData.size();
    }

    @Override
    public ObjDoiThuong.ArrCardsBean.ProductInfo getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.card_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjDoiThuong.ArrCardsBean.ProductInfo objApp = getItem(position);
        if(objApp==null) return;
        viewHolder.logo.setImageView(objApp.getMobile_card_img());
        switch (mType){
            case DoiThuongFragment.TYPE_VIETEL:
                viewHolder.value.setBackgroundResource(R.color.card_vietel);
                break;
            case DoiThuongFragment.TYPE_VINA:
                viewHolder.value.setBackgroundResource(R.color.card_vinaphone);
                break;
            case DoiThuongFragment.TYPE_MOBI:
                viewHolder.value.setBackgroundResource(R.color.card_mobifone);
                break;
            case DoiThuongFragment.TYPE_VIETNAM_MOBILE:
                viewHolder.value.setBackgroundResource(R.color.card_vietnam_mobile);
                break;
            case DoiThuongFragment.TYPE_GFONE:
                viewHolder.value.setBackgroundResource(R.color.card_gfone);
                break;
            case DoiThuongFragment.TYPE_SFONE:
                viewHolder.value.setBackgroundResource(R.color.card_sfone);
                break;
        }
        long coin = Long.parseLong(objApp.getMobile_card_price());
        viewHolder.value.setText(CommonFunc.formatedCoin(coin)+ " VND");

    }


    public void notifyData(ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> arrApps) {
        mArrData = arrApps;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        ImageViewLoadingRec logo;
        TextView value;
        public ViewHolder(View parent) {

            logo = (ImageViewLoadingRec) parent.findViewById(R.id.logo);
            value = (TextView) parent.findViewById(R.id.value);
        }
    }
}
