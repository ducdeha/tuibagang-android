package com.kiemtien.tuibagang.subscreens.kiemtien.guilddialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/20/16.
 */
public class GuideAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mArrData ;
    private LayoutInflater mLayoutInflater;

    public GuideAdapter(Context context, List<String> arrApps) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public String getItem(int position) {
        return mArrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.guide_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder,position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position){
       viewHolder.position.setText(position+1+"");
        viewHolder.content.setText(getItem(position));
    }

    public void notifyData(List<String> arrApps){
        mArrData = arrApps;
        notifyDataSetChanged();
    }

    public class ViewHolder {
      TextView position, content;
        public ViewHolder(View parent) {

            position = (TextView) parent.findViewById(R.id.position);
            content = (TextView) parent.findViewById(R.id.content);
        }
    }
}
