package com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong;

/**
 * Created by DucNT on 4/23/16.
 */
public class ObjLsNhanThuong {


    /**
     * mobile_card_history_id : 3
     * date_time : 22/04/2016 15:32:38
     * mobile_card_type_name : Viettel
     * price : 100000
     * status : 1
     * card : {"date_time":"01/01/1970 08:00:00","number_code":"127341424123","seri":"512345712341"}
     */

    private String mobile_card_history_id;
    private String date_time;
    private String mobile_card_type_name;
    private String price;
    private int status;
    /**
     * date_time : 01/01/1970 08:00:00
     * number_code : 127341424123
     * seri : 512345712341
     */

    private CardBean card;

    public String getMobile_card_history_id() {
        return mobile_card_history_id;
    }

    public void setMobile_card_history_id(String mobile_card_history_id) {
        this.mobile_card_history_id = mobile_card_history_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getMobile_card_type_name() {
        return mobile_card_type_name;
    }

    public void setMobile_card_type_name(String mobile_card_type_name) {
        this.mobile_card_type_name = mobile_card_type_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CardBean getCard() {
        return card;
    }

    public void setCard(CardBean card) {
        this.card = card;
    }

    public static class CardBean {
        private String date_time;
        private String number_code;
        private String seri;

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getNumber_code() {
            return number_code;
        }

        public void setNumber_code(String number_code) {
            this.number_code = number_code;
        }

        public String getSeri() {
            return seri;
        }

        public void setSeri(String seri) {
            this.seri = seri;
        }
    }
}
