package com.kiemtien.tuibagang.subscreens.events;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by DucNT on 5/4/16.
 */
public class AnswerDialog extends Dialog implements Html.ImageGetter {
    private Context mContext;
    private ObjLogin mObjLogin;
    private QuestionDoneCallback mQuestionDoneCallback;
    private  TextView mOkBtn;

    public AnswerDialog(Context context, String title, ObjEvents.ArrEventsBean objEvent) {
        super(context);
        mContext = context;
        mObjLogin = SharePreferences.getUserInfo(mContext);
        initView(title, objEvent);
    }


    private void initView(String title, final ObjEvents.ArrEventsBean objEvent) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.answer_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(true);
        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvContent = (TextView) findViewById(R.id.content);
        mOkBtn = (TextView) findViewById(R.id.ok);
        TextView cancelBtn = (TextView) findViewById(R.id.back);
        final EditText input = (EditText) findViewById(R.id.input_box);
        tvContent.setMovementMethod(new ScrollingMovementMethod());

        //DucNT: fillData:
        final int eventType = Integer.parseInt(objEvent.getEvent_type());
        boolean isAlertType = objEvent.getEvent_type().equalsIgnoreCase("0");
        if (isAlertType) {
            input.setVisibility(View.GONE);
            mOkBtn.setVisibility(View.GONE);
        }
        tvTitle.setText(objEvent.getEvent_title());

        UrlImageParser p = new UrlImageParser(tvContent, mContext);
        Spanned htmlSpan = Html.fromHtml(objEvent.getEvent_info(), this, null);
        tvContent.setText(htmlSpan);

//        tvContent.setText(Html.fromHtml(objEvent.getEvent_info(),this,null));
        if(eventType==2){
            mOkBtn.setText("Chia sẻ");
            input.setVisibility(View.GONE);
        }
        if(eventType==3){
            mOkBtn.setText("Xem ngay");
            input.setVisibility(View.GONE);
        }
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PermissionUtil.checkPermissionReadPhoneState((Activity) mContext)) {
                    return;
                }
                BaseRestClient.sendDeviceInfo(mContext,"Tham gia Event","Tham gia Event: "+ objEvent.getEvent_info(),"0");
                if (eventType == 1) {
                    String answer = input.getText().toString();
                    if (TextUtils.isEmpty(answer)) return;
                    callApiFeedBack(answer, objEvent.getEvent_id());
                } else if (eventType == 2) {
                    //Share
                    callApiShare(objEvent.getEvent_id());
                    String shareContent = objEvent.getEvent_info()+ ". Để biết thêm chi tiết, vui lòng truy cập: " +objEvent.getEvent_link_share().replaceAll("--code--",mObjLogin.getShare_code());
                    if(!TextUtils.isEmpty(objEvent.getEvent_note())) shareContent = shareContent +".\nLưu ý: "+ objEvent.getEvent_note();
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,shareContent);
                    mContext.startActivity(Intent.createChooser(sharingIntent, "Chia sẻ qua"));
                }else if ( eventType == 3){
                    //Diem danh hang ngay
                    String url = objEvent.getEvent_link_share();
                    if(!url.startsWith("http")) url = "http://"+url;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    mContext.startActivity(browserIntent);
                }
                dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void callApiFeedBack(String answer, String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.EVENT_ID, id);
        params.put(ApiKey.CONTENT, answer);
        BaseRestClient.get(mContext, ApiUrl.URL_EVENT_FEEDBACK, params, new DataResponse(mContext) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.alert), mContext.getString(R.string.done_question));
                if (mQuestionDoneCallback != null) mQuestionDoneCallback.Successful(true);
            }
        });
    }

    private void callApiShare( String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.EVENT_ID, id);
        BaseRestClient.get(mContext, ApiUrl.URL_SHARE_EVENT, params, new DataResponse(mContext) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject object = new JSONObject(response);
                    String coin = object.getString("coin");
                    ((MainActivity) mContext).setToolbarMoney(coin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public void setOnQuestionDoneListener(QuestionDoneCallback onQuestionDoneListener) {
        mQuestionDoneCallback = onQuestionDoneListener;
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable drawable = null;
        drawable = getDrawableFromUrl(source);
        d.addLevel(0, 0, drawable);
        d.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        return d;
    }

    public static Drawable getDrawableFromUrl(String path) {
        try {
            URL url = new URL(path.replaceAll(" ","%20"));
            InputStream inputStream = url.openStream();
            Bitmap b = BitmapFactory.decodeStream(inputStream);
            b.setDensity(Bitmap.DENSITY_NONE);
            Drawable d = new BitmapDrawable(b);
            return d;
        } catch (IOException ignored) {
        }
        return null;
    }


    public interface QuestionDoneCallback {
        void Successful(boolean isSuccess);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.MY_PERMISSION_READ_PHONE&&mOkBtn!=null) mOkBtn.performLongClick();
    }
}
