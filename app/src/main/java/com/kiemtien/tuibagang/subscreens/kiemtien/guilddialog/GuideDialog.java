package com.kiemtien.tuibagang.subscreens.kiemtien.guilddialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.OnInternetButtonClickListener;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.kiemtien.KiemTienAdapter;
import com.kiemtien.tuibagang.subscreens.kiemtien.ObjKiemTien;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by DucNT on 4/21/16.
 */
public class GuideDialog extends Dialog {
    private Context mContext;
    private ObjKiemTien.ArrAppBean mAppInfo;
    private ImageViewLoading mAvatar;
    private TextView mTitle, mInstallCount, mCoin, mNotice, mCaiDat, mTroLai;
    private ListView mListSteps;
    private List<String> mArrData;
    private ObjLogin mObjLogin;
    private boolean mCanGetCoin;
    private InstallAppListener mInstallAppListener;
    private KiemTienAdapter mKiemTienAdapter;

    public GuideDialog(Context context, ObjKiemTien.ArrAppBean appBean, KiemTienAdapter kiemTienAdapter) {
        super(context);
        mContext = context;
        mAppInfo = appBean;
        mArrData = appBean.getDesc().getArr_steps();
        mObjLogin = SharePreferences.getUserInfo(mContext);
        mKiemTienAdapter = kiemTienAdapter;
        initView();
        fillData();
    }

    private void initView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.install_app_guide);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(true);
        mListSteps = (ListView) findViewById(R.id.list_view);
        mAvatar = (ImageViewLoading) findViewById(R.id.avatar);
        mTitle = (TextView) findViewById(R.id.title);
        mInstallCount = (TextView) findViewById(R.id.install_count);
        mCoin = (TextView) findViewById(R.id.coin);
        mNotice = (TextView) findViewById(R.id.notice);
        mTroLai = (TextView) findViewById(R.id.trolai);
        mCaiDat = (TextView) findViewById(R.id.caidat);
    }

    private void fillData() {
        GuideAdapter mGuideAdapter = new GuideAdapter(mContext, mArrData);
        mListSteps.setAdapter(mGuideAdapter);
        mAvatar.setImageAsAvatar(mAppInfo.getApp_image(), mContext);
        mTitle.setText(mAppInfo.getApp_name());
        mInstallCount.setText(mContext.getResources().getString(R.string.install_count, mAppInfo.getInstall_count()));
        mCoin.setText("+ " + mAppInfo.getCost_per_install() + " Xu");
        mNotice.setText("Chú ý: " + mAppInfo.getDesc().getNotice());
        if (Integer.parseInt(mAppInfo.getStatus()) > 0 && CommonFunc.isAppInstalled(mContext, mAppInfo.getApp_package())) {
            //Da cai dat
            mCaiDat.setText(mContext.getResources().getString(R.string.get_coin));
            mCanGetCoin = true;
        } else {
            mCaiDat.setText(mContext.getResources().getString(R.string.install_btn));
        }
        mCaiDat.setOnClickListener(new OnInternetButtonClickListener(mContext) {
            @Override
            public void onButtonClicked(View v) {
                if (mCanGetCoin)
                    callApiGetCoin();
                else
                    callApiInstallApp();
                dismiss();
            }
        });
        mTroLai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void callApiInstallApp() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.ACCEPT_TOKEN, mObjLogin.getAccept_token());
        params.put(ApiKey.PUBLISHER_CAMPAIGN_ID, mAppInfo.getCampaign_id());
        BaseRestClient.post(mContext, ApiUrl.URL_INSTALL_APP, params, new DataResponse(mContext) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                BaseRestClient.sendDeviceInfo(mContext,"Tải app","Tải ứng dụng: "+ mAppInfo.getApp_name(),"0");
                if (mInstallAppListener != null)
                    mInstallAppListener.onInstallApp("1");

                if (mAppInfo.getCus_option_install().equalsIgnoreCase("0")) {
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("market://search?q=" + mAppInfo.getKey_word()));
                    mContext.startActivity(goToMarket);

                } else {
                    String url = "market://details?id=" + mAppInfo.getApp_package();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    mContext.startActivity(intent);
                }
            }
        });
    }

    private void callApiGetCoin() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.ACCEPT_TOKEN, mObjLogin.getAccept_token());
        params.put(ApiKey.PUBLISHER_CAMPAIGN_ID, mAppInfo.getCampaign_id());
        BaseRestClient.post(mContext, ApiUrl.URL_GET_COIN, params, new DataResponse(mContext) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject object = new JSONObject(response);
                    boolean result = object.getBoolean("result");
                    String coin = object.getString("coin");
                    ((MainActivity) mContext).setToolbarMoney(coin);
                    String status = object.getString("status");
                    if (result && mInstallAppListener != null) {
                        mInstallAppListener.onInstallApp(status);
                    }
                    String message = object.getString("message");
                    if (!TextUtils.isEmpty(message))
                        CommonFunc.showDialogOneButton(mContext, mContext.getResources().getString(R.string.alert), message);
                    String logCoin = object.getString("log_coin");
                    BaseRestClient.sendDeviceInfo(mContext,"Nhận thưởng","Nhận thưởng từ ứng dụng: "+ mAppInfo.getApp_name(),logCoin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setOnInstallAppListener(InstallAppListener installAppListener) {
        mInstallAppListener = installAppListener;
    }

    public interface InstallAppListener {
        void onInstallApp(String status);
    }
}
