package com.kiemtien.tuibagang.subscreens.canhan.cauhoithuonggap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/20/16.
 */
public class CauHoiThuongGapAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjCauHoiThuongGap> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ObjLogin mObjLogin;

    public CauHoiThuongGapAdapter(Context context, ArrayList<ObjCauHoiThuongGap> arrApps) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
        mObjLogin = SharePreferences.getUserInfo(mContext);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjCauHoiThuongGap getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.cau_hoi_thuong_gap_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjCauHoiThuongGap objApp = getItem(position);
        if (objApp == null) return;
        viewHolder.mTitle.setText(String.valueOf(position+1)+ ". "+objApp.getAdmin_faq_question());
        viewHolder.mContent.setText(objApp.getAdmin_faq_answer());

    }


    public class ViewHolder {
        TextView mTitle, mContent;

        public ViewHolder(View parent) {
            mTitle = (TextView) parent.findViewById(R.id.title);
            mContent = (TextView) parent.findViewById(R.id.content);
        }
    }
}
