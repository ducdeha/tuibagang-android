package com.kiemtien.tuibagang.subscreens.canhan;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/22/16.
 */
public class CaNhanRow extends RelativeLayout {
    public CaNhanRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.RowCaNhan);
        String text = "";
        Drawable resouece = null;
        boolean needUnderline = true;
        int indexCount = ta.length();
        try {

            for (int i = 0; i < indexCount; ++i) {
                final int attr = ta.getIndex(i);
                if (attr == R.styleable.RowCaNhan_text)
                    text = (String) ta.getText(attr);
                if (attr == R.styleable.RowCaNhan_source) {
                    resouece = ta.getDrawable(attr);
                }
                if (attr == R.styleable.RowCaNhan_underline) {
                    needUnderline = ta.getBoolean(attr, true);
                }

            }

        } finally {
            ta.recycle();
        }
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_ca_nhan, this, true);
        view.setBackground(getResources().getDrawable(R.drawable.selector_white_gray));
        TextView title = (TextView) view.findViewById(R.id.text);
        ImageView image = (ImageView) view.findViewById(R.id.img);
        View line = (View) view.findViewById(R.id.line);
        image.setImageDrawable(resouece);
        if (!needUnderline) line.setVisibility(View.GONE);
        title.setText(text);
        isInEditMode();
    }
}
