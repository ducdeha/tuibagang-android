package com.kiemtien.tuibagang.subscreens.canhan.topkiemtien;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;

/**
 * Created by DucNT on 4/20/16.
 */
public class TopRankAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjTopRank> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public TopRankAdapter(Context context, ArrayList<ObjTopRank> arrApps) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjTopRank getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.top, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjTopRank objApp = getItem(position);
        if (objApp == null) return;
        viewHolder.mPosition.setText(String.valueOf(position+1));
        viewHolder.mContent.setText(objApp.getPublisher_facebook_name());
        viewHolder.mCoin.setText(CommonFunc.formatedCoin(objApp.getCoin())+" Xu");
        if(position>2){
            viewHolder.mPosition.setTextSize(12);
            viewHolder.mPosition.setBackground(mContext.getResources().getDrawable(R.drawable.gray_badge_with_padding));
        }else {
            viewHolder.mPosition.setTextSize(16);
            viewHolder.mPosition.setBackground(mContext.getResources().getDrawable(R.drawable.orange_badge_with_padding));
        }

    }


    public class ViewHolder {
        TextView mPosition, mContent, mCoin;

        public ViewHolder(View parent) {
            mPosition = (TextView) parent.findViewById(R.id.position);
            mContent = (TextView) parent.findViewById(R.id.content);
            mCoin = (TextView) parent.findViewById(R.id.coin);
        }
    }
}
