package com.kiemtien.tuibagang.subscreens.canhan;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.OnInternetButtonClickListener;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;
import com.kiemtien.tuibagang.mabu.widget.TuiBaGangDialog;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.canhan.cauhoithuonggap.CauHoiThuongGapFragment;
import com.kiemtien.tuibagang.subscreens.canhan.gioithieu.GioiThieuFragment;
import com.kiemtien.tuibagang.subscreens.canhan.lienhe.LienHeFragment;
import com.kiemtien.tuibagang.subscreens.canhan.lshoatdong.LsHoatDongFragment;
import com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong.LsNhanThuongFragment;
import com.kiemtien.tuibagang.subscreens.canhan.topkiemtien.TopRankFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by DucNT on 4/22/16.
 */
public class CaNhanFragment extends Fragment {
    public static final int SHARE_BUTTON = 2;
    public static final int CANHAN_ROW_CLICKED = 3;
    private ImageView mCover;
    private ImageViewLoading mAvatar;
    private TextView mName, mCoin, mShareCode, mShareCodeButton;
    private ObjLogin mObjLogin;
    private CaNhanRow mNhapMa, mLsNhanThuong, mLsHoatDong, mTopKiemTien, mCauHoi, mLienHe, mGioiThieu;
    private Activity mActivity;
    private mCanhanRow mRowClicked;
    private int mButtonSendDevice = -1;

    private enum mCanhanRow {
        NHAP_MA, LS_NHAN_THUONG, LS_HOAT_DONG, TOP_KIEM_TIEN, CAU_HOI, LIEN_HE, GIOI_THIEU
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.canhan_fragment, container, false);
        mCover = (ImageView) view.findViewById(R.id.cover);
        mAvatar = (ImageViewLoading) view.findViewById(R.id.avatar);
        mName = (TextView) view.findViewById(R.id.name);
        mCoin = (TextView) view.findViewById(R.id.coin);
        mShareCode = (TextView) view.findViewById(R.id.share_code_input);
        mShareCodeButton = (TextView) view.findViewById(R.id.share_code);
        mNhapMa = (CaNhanRow) view.findViewById(R.id.nhapma);
        mLsNhanThuong = (CaNhanRow) view.findViewById(R.id.lsnt);
        mLsHoatDong = (CaNhanRow) view.findViewById(R.id.lshd);
        mTopKiemTien = (CaNhanRow) view.findViewById(R.id.top_kiem_tien);
        mCauHoi = (CaNhanRow) view.findViewById(R.id.cauhoi);
        mLienHe = (CaNhanRow) view.findViewById(R.id.lienhe);
        mGioiThieu = (CaNhanRow) view.findViewById(R.id.about);
        fillData();
        mNhapMa.setOnClickListener(clickCaNhanRow(mCanhanRow.NHAP_MA));
        mLsNhanThuong.setOnClickListener(clickCaNhanRow(mCanhanRow.LS_NHAN_THUONG));
        mLsHoatDong.setOnClickListener(clickCaNhanRow(mCanhanRow.LS_HOAT_DONG));
        mTopKiemTien.setOnClickListener(clickCaNhanRow(mCanhanRow.TOP_KIEM_TIEN));
        mCauHoi.setOnClickListener(clickCaNhanRow(mCanhanRow.CAU_HOI));
        mLienHe.setOnClickListener(clickCaNhanRow(mCanhanRow.LIEN_HE));
        mGioiThieu.setOnClickListener(clickCaNhanRow(mCanhanRow.GIOI_THIEU));
        mShareCodeButton.setOnClickListener(clickShareCode());
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) mActivity).mToolbar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) mActivity).mToolbar.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener clickShareCode() {
        return new OnInternetButtonClickListener(mActivity) {
            @Override
            public void onButtonClicked(View v) {
                if (!PermissionUtil.checkPermissionReadPhoneState(mActivity)) {
                    mButtonSendDevice = SHARE_BUTTON;
                    return;
                }
                BaseRestClient.sendDeviceInfo(mActivity, "Chia sẻ", "Chia sẻ mã: " + mObjLogin.getShare_code(), "0");
                String shareBody = getResources().getString(R.string.share_content, mObjLogin.getShare_code());
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Chia sẻ qua"));
            }
        };
    }

    private View.OnClickListener clickCaNhanRow(final mCanhanRow position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickRow(position);
            }
        };
    }

    private void clickRow(mCanhanRow position) {
        if (!PermissionUtil.checkPermissionReadPhoneState(mActivity)) {
            mButtonSendDevice = CANHAN_ROW_CLICKED;
            mRowClicked = position;
            return;
        }
        if (position == mCanhanRow.NHAP_MA) {
            final TuiBaGangDialog tuiBaGangDialog = new TuiBaGangDialog(mActivity, getResources().getString(R.string.nhap_ma), getResources().getString(R.string.nhap_ma_content),
                    new TuiBaGangDialog.OnPosibleClickListener() {
                        @Override
                        public void OnClick(String edtText) {
                            callApiInputCode(edtText);
                        }
                    });
            tuiBaGangDialog.setHindEditText("Nhập mã tại đây");
            CommonFunc.showSoftKeyboard(mActivity);
            tuiBaGangDialog.show();
        } else if (position == mCanhanRow.LS_NHAN_THUONG) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(LsNhanThuongFragment.class, new Bundle(), false);
        } else if (position == mCanhanRow.LS_HOAT_DONG) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(LsHoatDongFragment.class, new Bundle(), false);
        } else if (position == mCanhanRow.TOP_KIEM_TIEN) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(TopRankFragment.class, new Bundle(), false);
        } else if (position == mCanhanRow.CAU_HOI) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(CauHoiThuongGapFragment.class, new Bundle(), false);
        } else if (position == mCanhanRow.LIEN_HE) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(LienHeFragment.class, new Bundle(), false);
        } else if (position == mCanhanRow.GIOI_THIEU) {
            ((MainActivity) mActivity).goToFragmentClearBackStack(GioiThieuFragment.class, new Bundle(), false);
        }
    }

    private void callApiInputCode(final String text) {
        if (TextUtils.isEmpty(text)) return;
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.SHARE_CODE, text);
        BaseRestClient.get(mActivity, ApiUrl.URL_SHARE_CODE, params, new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject object = new JSONObject(response);
                    String msg = object.getString("message");
                    String coin = object.getString("coin");
                    String logCoin = object.getString("log_coin");
                    BaseRestClient.sendDeviceInfo(mContext, "Nhập mã chia sẻ", "Nhập mã :" + text, logCoin);
                    ((MainActivity) mActivity).setToolbarMoney(String.valueOf(coin));
                    mCoin.setText(String.valueOf(coin));
                    CommonFunc.showDialogOneButton(mActivity, getResources().getString(R.string.nav_thongbao), msg, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    mNhapMa.setVisibility(View.GONE);
                    mObjLogin.setShare_code_status("1");
                    Gson gson = new Gson();
                    String strLogin = gson.toJson(mObjLogin);
                    SharePreferences.saveStringPreference(mContext, SharePreferences.KEY_PREF_USER_INFO, strLogin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void fillData() {
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        final String avatar = mObjLogin.getPublisher_avatar_url();
        mCoin.setText(CommonFunc.formatedCoin(mObjLogin.getCoin()) + " Xu");
        mShareCode.setText(mObjLogin.getShare_code());
        mName.setText(mObjLogin.getPublisher_facebook_name());
        mAvatar.setCircleImg(avatar, mActivity);
        if (mObjLogin.getShare_code_status().equalsIgnoreCase("1"))
            mNhapMa.setVisibility(View.GONE);
        Glide.with(getContext())
                .load(avatar).asBitmap()
                .fitCenter()
                .into(new BitmapImageViewTarget(mCover) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        mCover.setImageBitmap(fastblur(resource, 0.5f, 25));
                    }
                });
    }

    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {
        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        if (radius < 1) {
            return (null);
        }
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return (bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.MY_PERMISSION_READ_PHONE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (mButtonSendDevice == SHARE_BUTTON) mShareCodeButton.performLongClick();
            if (mButtonSendDevice == CANHAN_ROW_CLICKED) clickRow(mRowClicked);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
