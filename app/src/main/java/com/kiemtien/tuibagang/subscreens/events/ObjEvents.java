package com.kiemtien.tuibagang.subscreens.events;

import java.util.List;

/**
 * Created by DucNT on 4/21/16.
 */
public class ObjEvents {

    /**
     * returnVal : 1
     * arr_events : [{"event_id":"1","event_icon":"http://45.32.58.106:3333/img/logo.png","event_title":"Chia sẻ ứng dụng Túi Ba Gang","event_info":"Mời bạn bè sử dụng Túi Ba Gang bằng mã chia sẻ của mình để nhận phần quà hấp dẫn","event_coin":"500","event_type":"alert"}]
     */

    private int returnVal;
    /**
     * event_id : 1
     * event_icon : http://45.32.58.106:3333/img/logo.png
     * event_title : Chia sẻ ứng dụng Túi Ba Gang
     * event_info : Mời bạn bè sử dụng Túi Ba Gang bằng mã chia sẻ của mình để nhận phần quà hấp dẫn
     * event_coin : 500
     * event_type : alert
     */

    private List<ArrEventsBean> arr_events;

    public int getReturnVal() {
        return returnVal;
    }

    public void setReturnVal(int returnVal) {
        this.returnVal = returnVal;
    }

    public List<ArrEventsBean> getArr_events() {
        return arr_events;
    }

    public void setArr_events(List<ArrEventsBean> arr_events) {
        this.arr_events = arr_events;
    }

    public static class ArrEventsBean {
        private String event_id;
        private String event_icon;
        private String event_title;
        private String event_info;
        private String event_coin;
        private String event_type;
        private String event_link_share;
        private String event_note;

        public String getEvent_link_share() {
            return event_link_share;
        }

        public void setEvent_link_share(String event_link_share) {
            this.event_link_share = event_link_share;
        }

        public String getEvent_note() {
            return event_note;
        }

        public void setEvent_note(String event_note) {
            this.event_note = event_note;
        }

        public String getEvent_id() {
            return event_id;
        }

        public void setEvent_id(String event_id) {
            this.event_id = event_id;
        }

        public String getEvent_icon() {
            return event_icon;
        }

        public void setEvent_icon(String event_icon) {
            this.event_icon = event_icon;
        }

        public String getEvent_title() {
            return event_title;
        }

        public void setEvent_title(String event_title) {
            this.event_title = event_title;
        }

        public String getEvent_info() {
            return event_info;
        }

        public void setEvent_info(String event_info) {
            this.event_info = event_info;
        }

        public String getEvent_coin() {
            return event_coin;
        }

        public void setEvent_coin(String event_coin) {
            this.event_coin = event_coin;
        }

        public String getEvent_type() {
            return event_type;
        }

        public void setEvent_type(String event_type) {
            this.event_type = event_type;
        }
    }
}
