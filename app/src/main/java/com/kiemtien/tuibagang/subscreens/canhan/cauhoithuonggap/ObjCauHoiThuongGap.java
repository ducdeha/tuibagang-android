package com.kiemtien.tuibagang.subscreens.canhan.cauhoithuonggap;

/**
 * Created by DucNT on 4/26/16.
 */
public class ObjCauHoiThuongGap {


    /**
     * admin_faq_question : Câu hỏi *
     * admin_faq_answer : Câu hỏi *
     */

    private String admin_faq_question;
    private String admin_faq_answer;

    public String getAdmin_faq_question() {
        return admin_faq_question;
    }

    public void setAdmin_faq_question(String admin_faq_question) {
        this.admin_faq_question = admin_faq_question;
    }

    public String getAdmin_faq_answer() {
        return admin_faq_answer;
    }

    public void setAdmin_faq_answer(String admin_faq_answer) {
        this.admin_faq_answer = admin_faq_answer;
    }
}
