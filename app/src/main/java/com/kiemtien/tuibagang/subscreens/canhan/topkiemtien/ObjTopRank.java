package com.kiemtien.tuibagang.subscreens.canhan.topkiemtien;

/**
 * Created by DucNT on 4/25/16.
 */
public class ObjTopRank {


    /**
     * publisher_facebook_name : Đức Tiến Đức
     * coin : 90000000
     */

    private String publisher_facebook_name;
    private String coin;

    public String getPublisher_facebook_name() {
        return publisher_facebook_name;
    }

    public void setPublisher_facebook_name(String publisher_facebook_name) {
        this.publisher_facebook_name = publisher_facebook_name;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }
}
