package com.kiemtien.tuibagang.subscreens.canhan.cauhoithuonggap;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/23/16.
 */
public class CauHoiThuongGapFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private CauHoiThuongGapAdapter mCauhoiAdapter;
    private ArrayList<ObjCauHoiThuongGap> mArrData = new ArrayList<>();
    private ObjLogin mObjLogin;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
        ((MainActivity) mActivity).setToolbarTitle(getResources().getString(R.string.cau_hoi_thuong_gap));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) mActivity).setToolbarTitle("");
    }

    @Override
    public int getLayout() {
        return R.layout.kiemtien_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.content_view);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mCauhoiAdapter = new CauHoiThuongGapAdapter(mActivity, mArrData);
        mListView.setAdapter(mCauhoiAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
        mSwipyRefreshLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.white));
        mListView.setBackgroundColor(mActivity.getResources().getColor(R.color.white));
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
    }


    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mListView.setEnabled(false);
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    mArrData.clear();
                    loadData(false);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_CAU_HOI;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                Gson gson = new Gson();
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray arrHistory = object.getJSONArray("arr_faq");
                    for (int i = 0; i < arrHistory.length(); i++) {
                        ObjCauHoiThuongGap objLsHoatDong = gson.fromJson(arrHistory.get(i).toString(), ObjCauHoiThuongGap.class);
                        mArrData.add(objLsHoatDong);
                    }
                    mCauhoiAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }
}

