package com.kiemtien.tuibagang.subscreens.canhan.lshoatdong;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;

/**
 * Created by DucNT on 4/20/16.
 */
public class LsHoatDongAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjLsHoatDong> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ObjLogin mObjLogin;
    private boolean mIsLsHoatDong = false;

    public LsHoatDongAdapter(Context context, ArrayList<ObjLsHoatDong> arrApps, boolean isLsHoatDong) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
        mObjLogin = SharePreferences.getUserInfo(mContext);
        mIsLsHoatDong = isLsHoatDong;
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjLsHoatDong getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.ls_hoatdong_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjLsHoatDong objApp = getItem(position);
        if (objApp == null) return;
        viewHolder.mDateTime.setText(objApp.getDate());
        viewHolder.mContent.setText(mIsLsHoatDong?objApp.getContent():objApp.getTitle());
        if (mIsLsHoatDong) {
            viewHolder.mCoin.setText(mContext.getResources().getString(R.string.coin_format, CommonFunc.formatedCoin(objApp.getCoin())));
            viewHolder.mCoin.setVisibility(View.VISIBLE);
        } else viewHolder.mCoin.setVisibility(View.GONE);

    }


    public class ViewHolder {
        TextView mDateTime, mContent, mCoin;

        public ViewHolder(View parent) {
            mDateTime = (TextView) parent.findViewById(R.id.date_time);
            mContent = (TextView) parent.findViewById(R.id.content);
            mCoin = (TextView) parent.findViewById(R.id.coin);
        }
    }
}
