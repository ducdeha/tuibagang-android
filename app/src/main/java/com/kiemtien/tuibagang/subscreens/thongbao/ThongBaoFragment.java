package com.kiemtien.tuibagang.subscreens.thongbao;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.kiemtien.tuibagang.MainActivity;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.canhan.lshoatdong.LsHoatDongAdapter;
import com.kiemtien.tuibagang.subscreens.canhan.lshoatdong.ObjLsHoatDong;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/26/16.
 */
public class ThongBaoFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private LsHoatDongAdapter mLsHoatDongAdapter;
    private ArrayList<ObjLsHoatDong> mArrData = new ArrayList<>();
    private ObjLogin mObjLogin;

    //DucNT
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
    }

    @Override
    public int getLayout() {
        return R.layout.kiemtien_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.content_view);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mLsHoatDongAdapter = new LsHoatDongAdapter(mActivity, mArrData, false);
        mListView.setAdapter(mLsHoatDongAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
        mListView.setOnItemClickListener(clickItem());
    }

    private AdapterView.OnItemClickListener clickItem() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mArrData.size() == 0) return;
                ObjLsHoatDong obj = mArrData.get(position);
                ThongBaoDialog dialog = new ThongBaoDialog(mActivity,obj.getTitle(), obj.getContent());
                dialog.show();
            }
        };
    }

    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mListView.setEnabled(false);
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    mArrData.clear();
                    loadData(false);
                } else {
                    loadData(true);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.OFFSET, "" + mArrData.size());
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_THONGBAO;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                Gson gson = new Gson();
                ((MainActivity) mContext).setBadge("0");
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray arrHistory = object.getJSONArray("arr_notification");
                    for (int i = 0; i < arrHistory.length(); i++) {
                        ObjLsHoatDong objLsHoatDong = gson.fromJson(arrHistory.get(i).toString(), ObjLsHoatDong.class);
                        mArrData.add(objLsHoatDong);
                    }
                    mLsHoatDongAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }
}
