package com.kiemtien.tuibagang.subscreens.canhan.lienhe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DucNT on 5/5/16.
 */
public class ObjLienHe implements Comparable<ObjLienHe>{
public static final int TYPE_ME = 1;
public static final int  TYPE_ADMIN = 2;

    /**
     * content : Admin cho em hỏi làm thế nào để đổi thưởng?
     * status : ​1
     * time : 2016-05-04 00:00:00
     */


    private String content;
    private int status;
    private String time;
    private long timeInMili;
    private String id;

    public long getTimeInMili() {
        String myDate = time;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(myDate);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return timeInMili;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTimeInMili(long timeInMili) {
        this.timeInMili = timeInMili;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    @Override
    public int compareTo(ObjLienHe another) {
        long value =  this.getTimeInMili()-another.getTimeInMili();
        if (value > 0) {
            return 1;
        } else if (value == 0) {
            return 0;
        } else return -1;
    }
}
