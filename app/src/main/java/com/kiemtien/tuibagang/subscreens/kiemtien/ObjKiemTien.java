package com.kiemtien.tuibagang.subscreens.kiemtien;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DucNT on 4/20/16.
 */
public class ObjKiemTien {


    /**
     * coin : 200
     * arr_app : [{"campaign_id":"2","app_package":"apk1243","app_name":"http://dota2.com/install","cost_per_install":"100","install_count":"3454","status":"2","desc":{"arr_steps":["Bước 1","Bước 2","Bước 3"],"notice":"Chú ý thực hiện đầy đủ 3 bước để nhận phần thưởng"},"question":"1+1=?"},{"campaign_id":"3","app_package":"com.sferamex.mibebeyyo","app_name":"https://itunes.apple.com/us/app/mi-bebe-y-yo/id845182478?mt=8","cost_per_install":"5","install_count":"12+","status":0,"desc":{"arr_steps":[null,null,null],"notice":null},"question":null}]
     */

    private String coin;

    private int notification_count;
    /**
     * campaign_id : 2
     * app_package : apk1243
     * app_name : http://dota2.com/install
     * cost_per_install : 100
     * install_count : 3454
     * status : 2
     * desc : {"arr_steps":["Bước 1","Bước 2","Bước 3"],"notice":"Chú ý thực hiện đầy đủ 3 bước để nhận phần thưởng"}
     * question : 1+1=?
     */

    private List<ArrAppBean> arr_app;

    public int getNotification_count() {
        return notification_count;
    }

    public void setNotification_count(int notification_count) {
        this.notification_count = notification_count;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public List<ArrAppBean> getArr_app() {
        return arr_app;
    }

    public void setArr_app(List<ArrAppBean> arr_app) {
        this.arr_app = arr_app;
    }

    public static class ArrAppBean {
        private String campaign_id;
        private String app_package;
        private String app_name;
        private String cost_per_install;
        private String install_count;
        private String status;
        private String app_image;
        private String cus_option_install;
        private String realStatus;
        private String key_word;

        /**
         * arr_steps : ["Bước 1","Bước 2","Bước 3"]
         * notice : Chú ý thực hiện đầy đủ 3 bước để nhận phần thưởng
         */

        private DescBean desc;
        private String question;


        public String getKey_word() {
            return key_word;
        }

        public void setKey_word(String key_word) {
            this.key_word = key_word;
        }

        public String getRealStatus() {
            return realStatus;
        }

        public void setRealStatus(String realStatus) {
            this.realStatus = realStatus;
        }

        public String getCus_option_install() {
            return cus_option_install;
        }

        public void setCus_option_install(String cus_option_install) {
            this.cus_option_install = cus_option_install;
        }

        public String getApp_image() {
            return app_image;
        }

        public void setApp_image(String app_image) {
            this.app_image = app_image;
        }

        public String getCampaign_id() {
            return campaign_id;
        }

        public void setCampaign_id(String campaign_id) {
            this.campaign_id = campaign_id;
        }

        public String getApp_package() {
            return app_package;
        }

        public void setApp_package(String app_package) {
            this.app_package = app_package;
        }

        public String getApp_name() {
            return app_name;
        }

        public void setApp_name(String app_name) {
            this.app_name = app_name;
        }

        public String getCost_per_install() {
            return cost_per_install;
        }

        public void setCost_per_install(String cost_per_install) {
            this.cost_per_install = cost_per_install;
        }

        public String getInstall_count() {
            return install_count;
        }

        public void setInstall_count(String install_count) {
            this.install_count = install_count;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public DescBean getDesc() {
            return desc;
        }

        public void setDesc(DescBean desc) {
            this.desc = desc;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public static class DescBean {
            private String notice;
            private List<String> arr_steps;

            public String getNotice() {
                return notice;
            }

            public void setNotice(String notice) {
                this.notice = notice;
            }

            public List<String> getArr_steps() {
                return arr_steps;
            }

            public void setArr_steps(List<String> arr_steps) {
                this.arr_steps = arr_steps;
            }
        }
    }
}
