package com.kiemtien.tuibagang.subscreens.canhan.lienhe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.DateUtilities;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by DucNT on 5/5/16.
 * Trong lớp này bắt buộc phải override lại 2 hàm: getViewTypeCount và getItemViewType
 * nếu không thì sẽ bị lỗi dùng lại ViewHolder
 */
public class LienHeAdapter extends BaseAdapter {
    private ArrayList<ObjLienHe> mArrData = new ArrayList<>();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ObjLogin mObjLogin;

    public LienHeAdapter(Context context, ArrayList<ObjLienHe> mArrData) {
        this.mArrData = mArrData;
        mContext = context;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mObjLogin = SharePreferences.getUserInfo(mContext);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjLienHe getItem(int position) {
        return mArrData.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /**
     * DucNT: Do type bắt dầu từ 0 mà status của mình lại bắt đầu từ 1 nên phải -1
     */
    @Override
    public int getItemViewType(int position) {
        return mArrData.get(position).getStatus() - 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        boolean isMe = getItemViewType(position) == 0;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(isMe ? R.layout.contact_item_me : R.layout.contact_item_admin, null);
            viewHolder.mAvatar = (ImageViewLoading) convertView.findViewById(R.id.avatar);
            viewHolder.mContent = (TextView) convertView.findViewById(R.id.content);
            viewHolder.mDateTime = (TextView) convertView.findViewById(R.id.date_time);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();
        fillData(viewHolder, position, isMe);
        return convertView;
    }

    private void fillData(ViewHolder viewHolder, int position, boolean isMe) {
        ObjLienHe objLienHe = mArrData.get(position);
        viewHolder.mDateTime.setText(DateUtilities.convertDate(objLienHe.getTime(),"dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm"));
        viewHolder.mContent.setText(objLienHe.getContent());
        viewHolder.mAvatar.setCircleImg(isMe ? mObjLogin.getPublisher_avatar_url() : "", mContext);
    }

    public void notifyDataAndSort(){
        Collections.sort(mArrData);
        notifyDataSetChanged();
    }


    public class ViewHolder {
        private ImageViewLoading mAvatar;
        private TextView mContent, mDateTime;
        public ViewHolder() {

        }
    }
}
