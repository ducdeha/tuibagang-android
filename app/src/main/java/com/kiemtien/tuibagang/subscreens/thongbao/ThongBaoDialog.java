package com.kiemtien.tuibagang.subscreens.thongbao;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.events.ObjEvents;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by DucNT on 5/4/16.
 */
public class ThongBaoDialog extends Dialog {
    private Context mContext;
    private ObjLogin mObjLogin;

    public ThongBaoDialog(Context context, String title, String msg) {
        super(context);
        mContext = context;
        mObjLogin = SharePreferences.getUserInfo(mContext);
        initView(title, msg);
    }

    private void initView(String title, String msg) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.thongbao_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(true);
        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvContent = (TextView) findViewById(R.id.content);
        TextView okBtn = (TextView) findViewById(R.id.ok);
        TextView cancelBtn = (TextView) findViewById(R.id.back);

        WebView webView = (WebView) findViewById(R.id.input_box);
        webView.setVisibility(View.VISIBLE);

        //DucNT: fillData:

        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        webView.setInitialScale(1);

//        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.setWebViewClient(new WebViewClient());

        okBtn.setVisibility(View.GONE);
        tvTitle.setText(title);
        tvContent.setVisibility(View.GONE);
        String mimeType = "text/html";
        final String encoding = "UTF-8";

        webView.loadDataWithBaseURL("",msg,mimeType,encoding,"");
        cancelBtn.setBackground(mContext.getResources().getDrawable(R.drawable.orange_shape_box));
        cancelBtn.setTextColor(mContext.getResources().getColor(R.color.white));
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
