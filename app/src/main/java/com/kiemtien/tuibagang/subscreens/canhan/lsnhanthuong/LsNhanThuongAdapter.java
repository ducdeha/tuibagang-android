package com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.OnInternetButtonClickListener;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DucNT on 4/20/16.
 */
public class LsNhanThuongAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjLsNhanThuong> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ObjLogin mObjLogin;
    private LsNhanThuongFragment mLsNhanThuongFragment;

    public LsNhanThuongAdapter(Context context, ArrayList<ObjLsNhanThuong> arrApps, LsNhanThuongFragment lsNhanThuongFragment) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
        mObjLogin = SharePreferences.getUserInfo(mContext);
        mLsNhanThuongFragment = lsNhanThuongFragment;
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjLsNhanThuong getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.lsdoithuong_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjLsNhanThuong objApp = getItem(position);
        if (objApp == null) return;
        //DucNT: View chung
        viewHolder.mDateTime.setText(objApp.getDate_time());
        viewHolder.mCardName.setText("Thẻ " + objApp.getMobile_card_type_name() + " " +
                CommonFunc.formatedCoin(Long.parseLong(objApp.getPrice())) + " " + mContext.getResources().getString(R.string.money_digit));
        int status = objApp.getStatus();
        if (status == 0) {
            //DucNT: Cho duyet
            viewHolder.mStatus.setText(mContext.getResources().getString(R.string.trade_waiting));
            viewHolder.mStatus.setTextColor(mContext.getResources().getColor(R.color.trade_waiting));
            viewHolder.mCancelBtn.setBackground(mContext.getResources().getDrawable(R.drawable.orange_shape_box_no_stroke));
            viewHolder.mCancelBtn.setVisibility(View.VISIBLE);
            viewHolder.mStatus.setVisibility(View.VISIBLE);
            viewHolder.mCancelBtn.setText("Hủy");
            viewHolder.mCancelBtn.setEnabled(true);
            viewHolder.mCardInfo.setVisibility(View.GONE);
            viewHolder.mCardName.setTextColor(mContext.getResources().getColor(R.color.black));
            viewHolder.mLineTop.setBackgroundColor(mContext.getResources().getColor(R.color.trade_waiting));
            viewHolder.mLineBottom.setBackgroundColor(mContext.getResources().getColor(R.color.trade_waiting));

        } else if (status == 1) {
            //DucNT: Doi the thanh cong
            ObjLsNhanThuong.CardBean objCard = objApp.getCard();
            viewHolder.mStatus.setText(mContext.getResources().getString(R.string.trade_successfully));
            viewHolder.mStatus.setTextColor(mContext.getResources().getColor(R.color.trade_successfully));
            viewHolder.mCancelBtn.setBackground(mContext.getResources().getDrawable(R.drawable.orange_shape_box_no_stroke));
            viewHolder.mCancelBtn.setVisibility(View.GONE);
            viewHolder.mCardInfo.setVisibility(View.VISIBLE);
            viewHolder.mAcceptDate.setText(objCard.getDate_time());
            viewHolder.mCardNumber.setText(Html.fromHtml(mContext.getResources().getString(R.string.card_number, objCard.getNumber_code())));
            viewHolder.mCardSeri.setText(Html.fromHtml(mContext.getResources().getString(R.string.card_seri, objCard.getSeri())));
            viewHolder.mLineTop.setBackgroundColor(mContext.getResources().getColor(R.color.trade_successfully));
            viewHolder.mLineBottom.setBackgroundColor(mContext.getResources().getColor(R.color.trade_successfully));

        } else if (status == 2) {
            //DucNT: Da huy
            viewHolder.mStatus.setVisibility(View.GONE);
            viewHolder.mCardName.setTextColor(mContext.getResources().getColor(R.color.trade_canceled));
            viewHolder.mDateTime.setTextColor(mContext.getResources().getColor(R.color.trade_canceled));
            viewHolder.mCardInfo.setVisibility(View.GONE);
            viewHolder.mLineTop.setBackgroundColor(mContext.getResources().getColor(R.color.trade_canceled));
            viewHolder.mLineBottom.setBackgroundColor(mContext.getResources().getColor(R.color.trade_canceled));
            disableCancelBtn(viewHolder);
        }
        viewHolder.mCancelBtn.setOnClickListener(cancelListener(viewHolder, objApp, position));
    }


    private void disableCancelBtn(ViewHolder viewHolder) {
        viewHolder.mCancelBtn.setBackground(mContext.getResources().getDrawable(R.drawable.gray_stroke_rec));
        viewHolder.mCancelBtn.setEnabled(false);
        viewHolder.mCancelBtn.setVisibility(View.VISIBLE);
        viewHolder.mStatus.setVisibility(View.GONE);
        viewHolder.mCardName.setTextColor(mContext.getResources().getColor(R.color.trade_canceled));
        viewHolder.mDateTime.setTextColor(mContext.getResources().getColor(R.color.trade_canceled));
        viewHolder.mCardInfo.setVisibility(View.GONE);
        viewHolder.mLineTop.setBackgroundColor(mContext.getResources().getColor(R.color.trade_canceled));
        viewHolder.mLineBottom.setBackgroundColor(mContext.getResources().getColor(R.color.trade_canceled));
        viewHolder.mCancelBtn.setText("Đã hủy");
    }


    private View.OnClickListener cancelListener(final ViewHolder viewHolder, final ObjLsNhanThuong objApp, final int position) {
        return new OnInternetButtonClickListener(mContext) {
            @Override
            public void onButtonClicked(View v) {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiKey.MOBILE_CARD_HISTORY_ID, objApp.getMobile_card_history_id());
                params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
                BaseRestClient.post(mContext, ApiUrl.URL_CANCEL_REQUEST, params, new DataResponse(mContext) {
                    @Override
                    public void onRealSuccess(String response) {
                        super.onRealSuccess(response);
                        mArrData.get(position).setStatus(2);
                        notifyDataSetChanged();
                        try {
                            JSONObject object = new JSONObject(response);
                            String coin = object.getString("coin");
                            if (!TextUtils.isEmpty(coin))
                                ((MainActivity) mContext).setToolbarMoney(coin);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNotSuccess() {
                        super.onNotSuccess();
                        mLsNhanThuongFragment.reloadData();
                    }
                });

            }
        };
    }

    public void notifyData(ArrayList<ObjLsNhanThuong> arrApps) {
        mArrData = arrApps;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        TextView mDateTime, mCardName, mStatus, mCancelBtn;
        TextView mAcceptDate, mCardNumber, mCardSeri;
        RelativeLayout mCardInfo;
        View mLineTop, mLineBottom;

        public ViewHolder(View parent) {
            mDateTime = (TextView) parent.findViewById(R.id.date_time);
            mCardName = (TextView) parent.findViewById(R.id.card_name);
            mStatus = (TextView) parent.findViewById(R.id.status);
            mCancelBtn = (TextView) parent.findViewById(R.id.huy_btn);

            mAcceptDate = (TextView) parent.findViewById(R.id.ngay_duyet_the);
            mCardNumber = (TextView) parent.findViewById(R.id.ma_so_the);
            mCardSeri = (TextView) parent.findViewById(R.id.seri_the);

            mCardInfo = (RelativeLayout) parent.findViewById(R.id.card_info);
            mLineBottom = parent.findViewById(R.id.line_bottom);
            mLineTop = parent.findViewById(R.id.line_top);

        }
    }
}
