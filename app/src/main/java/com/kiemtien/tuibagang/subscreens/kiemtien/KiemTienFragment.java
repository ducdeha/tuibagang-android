package com.kiemtien.tuibagang.subscreens.kiemtien;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.kiemtien.guilddialog.GuideDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by DucNT on 4/20/16.
 */
public class KiemTienFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private KiemTienAdapter mKiemTienAdapter;
    private ArrayList<ObjKiemTien.ArrAppBean> mArrData = new ArrayList<>();
    private ObjLogin mObjLogin;
    private int mTempPosition = -1;
    private ObjKiemTien.ArrAppBean mTempAppInfo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
    }

    @Override
    public int getLayout() {
        return R.layout.kiemtien_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.content_view);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mKiemTienAdapter = new KiemTienAdapter(mActivity, mArrData);
        mListView.setAdapter(mKiemTienAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
        mListView.setOnItemClickListener(clickListItem());
    }

    private AdapterView.OnItemClickListener clickListItem() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final ObjKiemTien.ArrAppBean objAPp = mArrData.get(position);
                callApiClickItem(objAPp.getCampaign_id());
                if (Integer.parseInt(objAPp.getStatus()) > 2 && Integer.parseInt(objAPp.getStatus()) != 4) {
                    String url = "market://details?id=" + objAPp.getApp_package();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    mActivity.startActivity(intent);
                    return;
                }
                GuideDialog dialog = new GuideDialog(mActivity, mArrData.get(position), mKiemTienAdapter);
                dialog.setOnInstallAppListener(new GuideDialog.InstallAppListener() {
                    @Override
                    public void onInstallApp(String status) {
                        int intStatus = Integer.parseInt(status);
                        mTempPosition = position;
                        if (intStatus > 0) {
                            mKiemTienAdapter.notifyData(status, mTempPosition);
                        }

                        mTempAppInfo = objAPp;

//                        mArrData.clear();
//                        loadData(false);
                    }
                });
                dialog.show();
            }
        };
    }

    private void callApiClickItem(String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.PUBLISHER_CAMPAIGN_ID, id);
        BaseRestClient.get(mActivity, ApiUrl.URL_CLICK_APP, params, new DataResponse(mActivity), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        //DucNT: Check app thay đổi trạng thái khi bấm nút cài đặt
        if (mTempAppInfo == null) return;
        String status = mTempAppInfo.getStatus();
        String realStatus = "";
        if (CommonFunc.isAppInstalled(mActivity, mTempAppInfo.getApp_package())) realStatus = "1";
        else realStatus = "0";
        if (mTempPosition != -1 && !status.equalsIgnoreCase(realStatus)) {
            mKiemTienAdapter.notifyData(realStatus, mTempPosition);
            mTempPosition = -1;
        }
    }

    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mListView.setEnabled(false);
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    mArrData.clear();
                    loadData(false);
                } else {
                    loadData(true);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.OFFSET, "" + mArrData.size());
        params.put(ApiKey.PUBLISHER_SEX, "" + mObjLogin.getPublisher_sex());
        params.put(ApiKey.PUBLISHER_AGE, "" + mObjLogin.getPublisher_age());
        params.put(ApiKey.PUBLISHER_CITY, "" + mObjLogin.getPublisher_city());
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_GET_LIST_APP;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    Gson gson = new Gson();
                    ObjKiemTien objKiemTien = gson.fromJson(response, ObjKiemTien.class);
                    List<ObjKiemTien.ArrAppBean> arrData = objKiemTien.getArr_app();
                    for (int i = 0; i < arrData.size(); i++) {
                        ObjKiemTien.ArrAppBean objAPp = arrData.get(i);
                        objAPp.setRealStatus(objAPp.getStatus());
                        mArrData.add(objAPp);
                    }
                    mKiemTienAdapter.notifyData(mArrData);
                    callApiInstalledApp(mArrData);
                    ((MainActivity) mActivity).setToolbarMoney(objKiemTien.getCoin() + "");
                    ((MainActivity) mActivity).setBadge(objKiemTien.getNotification_count() + "");
                } catch (Exception e) {
                    Log.e("DucNT", "Something went wrong in KiemTienFragment");
                }
            }
        };
    }

    private void callApiInstalledApp(ArrayList<ObjKiemTien.ArrAppBean> mArrData) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < mArrData.size(); i++) {
            ObjKiemTien.ArrAppBean appInfo = mArrData.get(i);
            String appPackage = appInfo.getApp_package();
            int intStatus = Integer.parseInt(appInfo.getStatus());
            if (intStatus > 0 && CommonFunc.isAppInstalled(mActivity, appPackage)) {
                builder.append(appInfo.getCampaign_id()).append(",");
            }
        }
        String arrCampaignId = builder.toString();
        if (arrCampaignId.length() > 0) {
//            arrCampaignId = arrCampaignId.substring(0,arrCampaignId.length()-2);
            HashMap<String, String> params = new HashMap<>();
            params.put(ApiKey.CONTENT, arrCampaignId);
            params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
            BaseRestClient.get(mActivity, ApiUrl.URL_CHECK_INSTALL, params, new DataResponse(mActivity), false);
        }
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }
}
