package com.kiemtien.tuibagang.subscreens.events;

import android.text.Html;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;

/**
 * Created by DucNT on 4/20/16.
 * Company: Deha
 */
public class EventsAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjEvents.ArrEventsBean> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public EventsAdapter(Context context, ArrayList<ObjEvents.ArrEventsBean> arrApps) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjEvents.ArrEventsBean getItem(int position) {
        return mArrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.app_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.statusIcon.setVisibility(View.GONE);
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjEvents.ArrEventsBean objApp = getItem(position);
        viewHolder.title.setText(objApp.getEvent_title());
        viewHolder.status.setText(Html.fromHtml(objApp.getEvent_info()));
        viewHolder.coin.setText("+ " + objApp.getEvent_coin() + " Xu");
        viewHolder.avatar.setImageAsAvatar(objApp.getEvent_icon(), mContext);
    }

    public void notifyData(ArrayList<ObjEvents.ArrEventsBean> arrApps) {
        mArrData = arrApps;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        ImageViewLoading avatar;
        TextView title;
        TextView status;
        ImageView statusIcon;
        TextView coin;

        public ViewHolder(View parent) {
            avatar = (ImageViewLoading) parent.findViewById(R.id.avatar);
            statusIcon = (ImageView) parent.findViewById(R.id.status_icon);
            title = (TextView) parent.findViewById(R.id.title);
            status = (TextView) parent.findViewById(R.id.status);
            coin = (TextView) parent.findViewById(R.id.coin_button);
        }
    }
}
