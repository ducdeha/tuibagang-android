package com.kiemtien.tuibagang.subscreens.kiemtien;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;

/**
 * Created by DucNT on 4/20/16.
 */
public class KiemTienAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ObjKiemTien.ArrAppBean> mArrData = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public KiemTienAdapter(Context context, ArrayList<ObjKiemTien.ArrAppBean> arrApps) {
        mContext = context;
        mArrData = arrApps;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public ObjKiemTien.ArrAppBean getItem(int position) {
        if (mArrData.size() > position)
            return mArrData.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.app_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        bindData(holder, position);
        return convertView;
    }

    private void bindData(ViewHolder viewHolder, int position) {
        ObjKiemTien.ArrAppBean objApp = getItem(position);
        if (objApp == null) return;
        viewHolder.title.setText(objApp.getApp_name());
        int status = 0;
        try {
            status = Integer.parseInt(objApp.getStatus());
        } catch (Exception e) {
        }
        int realStatus = Integer.parseInt(objApp.getRealStatus());
        if (!CommonFunc.isAppInstalled(mContext, objApp.getApp_package())) {
            status = 0;
            objApp.setStatus("0");
        }
//        if(status>0&& realStatus>0) {status = realStatus; objApp.setStatus(String.valueOf(realStatus));}
        if (status == 0) {
            //DucNT: Chua cai dat
            viewHolder.status.setText(R.string.not_install);
            viewHolder.statusIcon.setImageResource(R.drawable.ico_download);
        } else if (status == 1 || status == 4) {
            //DucNT: Da cai dat nhung chua nhan thuong
            viewHolder.status.setText(R.string.installed);
            viewHolder.statusIcon.setImageResource(R.drawable.ico_done);
        } else if (status == 2) {
            //DucNT: Cho nhan thuong dot 2
            viewHolder.status.setText(R.string.installed_2nd);
            viewHolder.statusIcon.setImageResource(R.drawable.ico_done);
        } else if (status == 3) {
            //DucNT: Nhan thuong xong xuoi
            viewHolder.status.setText(R.string.done);
            viewHolder.statusIcon.setImageResource(R.drawable.ico_done);
        } else if (status == 5) {
            //DucNT: Du dieu kien nhung ko nhan thuong dung gio
            viewHolder.status.setText(R.string.het_han);
            viewHolder.statusIcon.setImageResource(R.drawable.ico_done);
        }
        viewHolder.coin.setText("+ " + objApp.getCost_per_install() + " Xu");
        viewHolder.avatar.setImageAsAvatar(objApp.getApp_image(), mContext);
    }

    public void notifyData(ArrayList<ObjKiemTien.ArrAppBean> arrApps) {
        mArrData = arrApps;
        notifyDataSetChanged();
    }

    public void notifyData(String status, int position) {
        mArrData.get(position).setStatus(status);
        notifyDataSetChanged();
    }

    public class ViewHolder {
        ImageViewLoading avatar;
        TextView title;
        TextView status;
        ImageView statusIcon;
        TextView coin;

        public ViewHolder(View parent) {
            avatar = (ImageViewLoading) parent.findViewById(R.id.avatar);
            statusIcon = (ImageView) parent.findViewById(R.id.status_icon);
            title = (TextView) parent.findViewById(R.id.title);
            status = (TextView) parent.findViewById(R.id.status);
            coin = (TextView) parent.findViewById(R.id.coin_button);
        }
    }
}
