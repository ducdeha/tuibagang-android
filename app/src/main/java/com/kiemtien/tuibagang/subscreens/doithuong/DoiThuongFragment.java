package com.kiemtien.tuibagang.subscreens.doithuong;

import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;

/**
 * Created by DucNT on 4/22/16.
 */

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;

/**
 * Created by DucNT on 4/20/16.
 */
public class DoiThuongFragment extends BaseLoadDataFragment {
    public static final int TYPE_VIETEL = 0;
    public static final int TYPE_VINA = 1;
    public static final int TYPE_MOBI = 2;
    public static final int TYPE_VIETNAM_MOBILE = 3;
    public static final int TYPE_GFONE = 4;
    public static final int TYPE_SFONE = 5;
    private TwoWayView mListViettel, mListVina, mListMobi, mListVietNamMobile, mListGphone, mListSphone;
    private DoiThuongAdapter mAdapterViettel, mAdapterVina, mAdapterMobi, mAdapterVietNamMobile, mAdapterGphone, mAdapterSfone;
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrViettel = new ArrayList<>();
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrVina = new ArrayList<>();
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrMobi = new ArrayList<>();
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrVietNamMobile = new ArrayList<>();
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrGphone = new ArrayList<>();
    private ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> mArrSfone = new ArrayList<>();
    private ObjDoiThuong mObjDoiThuong;
    private ObjLogin mObjLogin;
    private TextView mCoinCanGet, mTheVietel, mTheVina, mTheMobi, mTheVietnamMobile, mTheSfone, mTheGphone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
    }

    @Override
    public int getLayout() {
        return R.layout.doithuong_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mCoinCanGet = (TextView) parentView.findViewById(R.id.coin_can_get);
        mTheVietel = (TextView) parentView.findViewById(R.id.text_vietel);
        mTheVina = (TextView) parentView.findViewById(R.id.text_vinaphone);
        mTheMobi = (TextView) parentView.findViewById(R.id.text_mobifone);
        mTheVietnamMobile = (TextView) parentView.findViewById(R.id.text_vietnam_mobile);
        mTheSfone = (TextView) parentView.findViewById(R.id.text_sfone);
        mTheGphone = (TextView) parentView.findViewById(R.id.text_gmobile);

        //ListViews
        mListViettel = (TwoWayView) parentView.findViewById(R.id.list_vietel);
        mListVina = (TwoWayView) parentView.findViewById(R.id.list_vinaphone);
        mListMobi = (TwoWayView) parentView.findViewById(R.id.list_mobifone);
        mListVietNamMobile = (TwoWayView) parentView.findViewById(R.id.list_vietnam_mobile);
        mListGphone = (TwoWayView) parentView.findViewById(R.id.list_gmobile);
        mListSphone = (TwoWayView) parentView.findViewById(R.id.list_sfone);

        mAdapterViettel = new DoiThuongAdapter(mActivity, mArrViettel, TYPE_VIETEL);
        mAdapterVina = new DoiThuongAdapter(mActivity, mArrVina, TYPE_VINA);
        mAdapterMobi = new DoiThuongAdapter(mActivity, mArrMobi, TYPE_MOBI);
        mAdapterVietNamMobile = new DoiThuongAdapter(mActivity, mArrVietNamMobile, TYPE_VIETNAM_MOBILE);
        mAdapterGphone = new DoiThuongAdapter(mActivity, mArrGphone, TYPE_GFONE);
        mAdapterSfone = new DoiThuongAdapter(mActivity, mArrSfone, TYPE_SFONE);

        mListViettel.setAdapter(mAdapterViettel);
        mListVina.setAdapter(mAdapterVina);
        mListMobi.setAdapter(mAdapterMobi);
        mListVietNamMobile.setAdapter(mAdapterVietNamMobile);
        mListGphone.setAdapter(mAdapterGphone);
        mListSphone.setAdapter(mAdapterSfone);

        mListViettel.setOnItemClickListener(onClickGetCard(TYPE_VIETEL));
        mListVina.setOnItemClickListener(onClickGetCard(TYPE_VINA));
        mListMobi.setOnItemClickListener(onClickGetCard(TYPE_MOBI));
        mListVietNamMobile.setOnItemClickListener(onClickGetCard(TYPE_VIETNAM_MOBILE));
        mListGphone.setOnItemClickListener(onClickGetCard(TYPE_GFONE));
        mListSphone.setOnItemClickListener(onClickGetCard(TYPE_SFONE));

    }

    private AdapterView.OnItemClickListener onClickGetCard(final int type) {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String cardId = "";
                String cardInfo = "";
                switch (type) {
                    case TYPE_VIETEL:
                        cardId = mAdapterViettel.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterViettel.getItem(position).getMobile_card_type_name()+ " " +mAdapterViettel.getItem(position).getMobile_card_price();
                        break;
                    case TYPE_VINA:
                        cardId = mAdapterVina.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterVina.getItem(position).getMobile_card_type_name()+ " " +mAdapterVina.getItem(position).getMobile_card_price();
                        break;
                    case TYPE_MOBI:
                        cardId = mAdapterMobi.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterMobi.getItem(position).getMobile_card_type_name()+ " " +mAdapterMobi.getItem(position).getMobile_card_price();
                        break;
                    case TYPE_VIETNAM_MOBILE:
                        cardId = mAdapterVietNamMobile.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterVietNamMobile.getItem(position).getMobile_card_type_name()+ " " +mAdapterVietNamMobile.getItem(position).getMobile_card_price();
                        break;
                    case TYPE_GFONE:
                        cardId = mAdapterGphone.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterGphone.getItem(position).getMobile_card_type_name()+ " " +mAdapterGphone.getItem(position).getMobile_card_price();
                        break;
                    case TYPE_SFONE:
                        cardId = mAdapterSfone.getItem(position).getMobile_card_id();
                        cardInfo = mAdapterSfone.getItem(position).getMobile_card_type_name()+ " " +mAdapterSfone.getItem(position).getMobile_card_price();
                        break;
                }
                if (TextUtils.isEmpty(cardId)) return;
                //Luc truoc goi api ghi log o day, sau do truyen card info vào trong hàm callApigetCard
                //Sau do goi ham` ghi log sau khi getCard thanh cong
                callApiGetCard(cardId,cardInfo);
            }
        };
    }

    private void callApiGetCard(String cardId, final String cardInfo) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        params.put(ApiKey.MOBILE_CARD_ID, cardId);
        BaseRestClient.get(mActivity, ApiUrl.URL_GET_CARD, params, new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject object = new JSONObject(response);
                    String msg = object.getString("message");
                    String coin = object.getString("coin");
                    String logCoin = object.getString("log_coin");
                    CommonFunc.showDialogOneButton(mActivity,getResources().getString(R.string.nav_thongbao),msg);
                    mCoinCanGet.setText(mActivity.getResources().getString(R.string.coin_number_can_get, CommonFunc.formatedCoin(coin)));
                    ((MainActivity)mActivity).setToolbarMoney(coin);
                    BaseRestClient.sendDeviceInfo(mActivity,"Đổi thưởng",cardInfo,logCoin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_GET_LIST_CARD;
    }

    @Override
    public DataResponse getCallback() {
        mListViettel.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                Gson gson = new Gson();
                try{
                mObjDoiThuong = gson.fromJson(response, ObjDoiThuong.class);}catch (Exception e) {}
                if (mObjDoiThuong == null) return;
                mArrViettel = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getViettel();
                mArrVina = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getVinaphone();
                mArrMobi = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getMobiFone();
                mArrVietNamMobile = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getVietnamobile();
                mArrGphone = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getGmobile();
                mArrSfone = (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getSFone();

                if(mArrViettel==null) mArrViettel = new ArrayList<>();
                if(mArrVina==null) mArrVina = new ArrayList<>();
                if(mArrMobi==null) mArrMobi = new ArrayList<>();
                if(mArrVietNamMobile==null) mArrVietNamMobile = new ArrayList<>();
                if(mArrGphone==null) mArrGphone = new ArrayList<>();
                if(mArrSfone==null) mArrSfone = new ArrayList<>();


                mAdapterViettel.notifyData(mArrViettel);
                mAdapterVina.notifyData(mArrVina);
                mAdapterMobi.notifyData(mArrMobi);
                mAdapterVietNamMobile.notifyData(mArrVietNamMobile);
                mAdapterGphone.notifyData(mArrGphone);
                mAdapterSfone.notifyData(mArrSfone);

                //DucNT: Ẩn các text header nếu như list thẻ ko có data:
                if (mArrViettel.size() == 0) mTheVietel.setVisibility(View.GONE);
                if (mArrVina.size() == 0) mTheVina.setVisibility(View.GONE);
                if (mArrMobi.size() == 0) mTheMobi.setVisibility(View.GONE);
                if (mArrVietNamMobile.size() == 0) mTheVietnamMobile.setVisibility(View.GONE);
                if (mArrGphone.size() == 0) mTheGphone.setVisibility(View.GONE);
                if (mArrSfone.size() == 0) mTheSfone.setVisibility(View.GONE);
                long coin = Long.parseLong(mObjDoiThuong.getCoin());
                mCoinCanGet.setText(mActivity.getResources().getString(R.string.coin_number_can_get, CommonFunc.formatedCoin(coin)));
            }
        };
    }

    @Override
    public ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo> getArrData() {
        if(mObjDoiThuong==null||mObjDoiThuong.getArr_cards().getViettel()==null) return new ArrayList<>();
        return (ArrayList<ObjDoiThuong.ArrCardsBean.ProductInfo>) mObjDoiThuong.getArr_cards().getViettel();
    }

    @Override
    protected void resetOffset() {

    }
}

