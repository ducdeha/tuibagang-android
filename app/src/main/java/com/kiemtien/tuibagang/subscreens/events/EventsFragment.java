package com.kiemtien.tuibagang.subscreens.events;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.DataResponse;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by DucNT on 4/20/16.
 */

public class EventsFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private EventsAdapter mEventAdapter;
    private ArrayList<ObjEvents.ArrEventsBean> mArrData = new ArrayList<>();
    private ObjLogin mObjLogin;
    private AnswerDialog mAnswerDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        loadData(false);
    }

    @Override
    public int getLayout() {
        return R.layout.kiemtien_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.content_view);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mEventAdapter = new EventsAdapter(mActivity, mArrData);
        mListView.setAdapter(mEventAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
        mListView.setOnItemClickListener(clickListItem());
    }

    private AdapterView.OnItemClickListener clickListItem() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ObjEvents.ArrEventsBean objEvent = mArrData.get(position);
                mAnswerDialog = new AnswerDialog(mActivity, getResources().getString(R.string.input_title),
                        objEvent);
                mAnswerDialog.setOnQuestionDoneListener(new AnswerDialog.QuestionDoneCallback() {
                    @Override
                    public void Successful(boolean isSuccess) {
                        if (isSuccess) {
                            mArrData.clear();
                            loadData(false);
                        }
                    }
                });
                mAnswerDialog.show();
            }
        };
    }

    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                mListView.setEnabled(false);
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    mArrData.clear();
                    loadData(false);
                } else {
                    loadData(true);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.OFFSET, "" + mArrData.size());
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_GET_LIST_EVENTS;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                Gson gson = new Gson();
                ObjEvents objKiemTien = gson.fromJson(response, ObjEvents.class);
                List<ObjEvents.ArrEventsBean> arrData = objKiemTien.getArr_events();
                for (int i = 0; i < arrData.size(); i++) {
                    mArrData.add(arrData.get(i));
                }
                mEventAdapter.notifyData(mArrData);
            }
        };
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(mAnswerDialog!=null) mAnswerDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}