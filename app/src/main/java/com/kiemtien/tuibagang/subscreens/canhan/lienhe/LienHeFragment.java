package com.kiemtien.tuibagang.subscreens.canhan.lienhe;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.base.BaseLoadDataFragment;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.widget.MabuMultiStateView;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DucNT on 4/20/16.
 */
public class LienHeFragment extends BaseLoadDataFragment {
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private LienHeAdapter mLienHeAdapter;
    private ObjLogin mObjLogin;
    private ArrayList<ObjLienHe> mArrData = new ArrayList<>();
    private ImageView mSendBtn;
    private EditText mInputText;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjLogin = SharePreferences.getUserInfo(mActivity);
        ((MainActivity) mActivity).setToolbarTitle(getResources().getString(R.string.lienhe));
        loadData(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) mActivity).setToolbarTitle("");
    }

    @Override
    public void viewStateCallback(MabuMultiStateView.ViewState state) {
        super.viewStateCallback(state);
        if (state == MabuMultiStateView.ViewState.ERROR) {
            mErrorView.setVisibility(View.VISIBLE);
            mLoadingView.setVisibility(View.GONE);
            mNoDataView.setVisibility(View.GONE);
            mContentView.setVisibility(View.GONE);
        } else if (state == MabuMultiStateView.ViewState.EMPTY) {
            mErrorView.setVisibility(View.GONE);
            mLoadingView.setVisibility(View.GONE);
            mNoDataView.setVisibility(View.VISIBLE);
            mContentView.setVisibility(View.GONE);

        } else if (state == MabuMultiStateView.ViewState.CONTENT) {
            mErrorView.setVisibility(View.GONE);
            mLoadingView.setVisibility(View.GONE);
            mNoDataView.setVisibility(View.GONE);
            mContentView.setVisibility(View.VISIBLE);

        } else if (state == MabuMultiStateView.ViewState.LOADING) {
            mErrorView.setVisibility(View.GONE);
            mLoadingView.setVisibility(View.VISIBLE);
            mNoDataView.setVisibility(View.GONE);
            mContentView.setVisibility(View.GONE);

        }
    }

    @Override
    public int getLayout() {
        return R.layout.lienhe_fragment;
    }

    @Override
    public void setupView(View parentView) {
        mSwipyRefreshLayout = (SwipyRefreshLayout) parentView.findViewById(R.id.swipeListMessage);
        mSendBtn = (ImageView) parentView.findViewById(R.id.send_button);
        mListView = (ListView) parentView.findViewById(R.id.list_view);
        mInputText = (EditText) parentView.findViewById(R.id.message_input);
        mLienHeAdapter = new LienHeAdapter(mActivity, mArrData);
        mListView.setAdapter(mLienHeAdapter);
        mSwipyRefreshLayout.setOnRefreshListener(swipeRefresh());
        mSendBtn.setOnClickListener(clickSend());
    }

    private View.OnClickListener clickSend() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mInputText.getText().toString();
                if (TextUtils.isEmpty(text)) return;
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
                params.put(ApiKey.CONTENT, text);
                BaseRestClient.post(mActivity, ApiUrl.URL_CHATTING, params, new DataResponse(mActivity) {
                    @Override
                    public void onRealSuccess(String response) {
                        super.onRealSuccess(response);
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = new JSONObject(response);
                            ObjLienHe objLienHe = gson.fromJson(obj.getJSONObject("data").toString(), ObjLienHe.class);
                            mArrData.add(objLienHe);
                            mLienHeAdapter.notifyDataSetChanged();
                            viewStateCallback(MabuMultiStateView.ViewState.CONTENT);
                            mListView.setSelection(mLienHeAdapter.getCount() - 1);
                            mInputText.setText("");
                            mInputText.clearFocus();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            }
        };
    }


    private SwipyRefreshLayout.OnRefreshListener swipeRefresh() {
        mListView.setEnabled(false);
        return new SwipyRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    loadData(true);
                } else {
                    mArrData.clear();
                    loadData(false);
                }
            }
        };
    }

    @Override
    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiKey.OFFSET, "" + mArrData.size());
        params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
        return params;
    }

    @Override
    public String getURL() {
        return ApiUrl.URL_LIST_CONTACT;
    }

    @Override
    public DataResponse getCallback() {
        mSwipyRefreshLayout.setRefreshing(false);
        mListView.setEnabled(true);
        return new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject object = new JSONObject(response);
                    Gson gson = new Gson();
                    JSONArray arrData = object.getJSONArray("arr_contact");
                    for (int i = 0; i < arrData.length(); i++) {
                        boolean isDuplicate = false;
                        ObjLienHe objLienHe = gson.fromJson(arrData.get(i).toString(), ObjLienHe.class);
                        for (int j = 0; j < mArrData.size(); j++) {
                            ObjLienHe obj = mArrData.get(j);
                            if (obj.getId().equalsIgnoreCase(objLienHe.getId())) {
                                isDuplicate = true;
                            }
                        }
                        if (!isDuplicate)
                            mArrData.add(objLienHe);
                    }
                    mLienHeAdapter.notifyDataAndSort();
                    if (mArrData.size() > arrData.length() && arrData.length() > 0)
                        mListView.setSelection(arrData.length() - 1);
                } catch (Exception e) {
                    Log.e("DucNT", "Something went wrong in LienHeFragment");
                }
            }
        };
    }

    @Override
    public ArrayList<?> getArrData() {
        return mArrData;
    }

    @Override
    protected void resetOffset() {

    }
}
