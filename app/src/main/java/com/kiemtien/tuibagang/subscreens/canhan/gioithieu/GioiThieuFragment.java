package com.kiemtien.tuibagang.subscreens.canhan.gioithieu;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;

import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.mabu.utils.OnInternetButtonClickListener;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;

/**
 * Created by DucNT on 4/26/16.
 */
public class GioiThieuFragment extends Fragment {
    private TextView mSendViaGmail, mChatSkype;
    private TextView mWebsite, mFanpage;
    private Activity mActivity;
    private TextView mCallNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gioithieu_fragment, container, false);
        mActivity = getActivity();
        ((MainActivity) mActivity).setToolbarTitle(getResources().getString(R.string.gioi_thieu));
        mSendViaGmail = (TextView) view.findViewById(R.id.send_gmail_btn);
        mChatSkype = (TextView) view.findViewById(R.id.skype_chat);
        mWebsite = (TextView) view.findViewById(R.id.web_content);
        mFanpage = (TextView) view.findViewById(R.id.fanpage_content);
        mCallNumber = (TextView) view.findViewById(R.id.call_btn);
        mSendViaGmail.setOnClickListener(clickGmail());
        mChatSkype.setOnClickListener(clickSkype());
        mWebsite.setOnClickListener(clickWebsite());
        mFanpage.setOnClickListener(clickFanpage());
        mCallNumber.setOnClickListener(clickPhoneNumber());
        return view;
    }

    private View.OnClickListener clickPhoneNumber() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkPermissionCallPhone(mActivity) ){
                    callNumber();
                }
            }
        };
    }

    private void callNumber() {
        String phone = "01235607777";
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionUtil.MY_PERMISSION_CALL_PHONE) {
            callNumber();
        }
    }

    private View.OnClickListener clickFanpage() {
        return new OnInternetButtonClickListener(mActivity) {
            @Override
            public void onButtonClicked(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/T%C3%BAi-Ba-Gang-Ki%E1%BA%BFm-ti%E1%BB%81n-Online-1038850976198188/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        };
    }

    private View.OnClickListener clickWebsite() {

        return new OnInternetButtonClickListener(mActivity) {
            @Override
            public void onButtonClicked(View v) {
                Uri uri = Uri.parse(getResources().getString(R.string.url_website));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        };
    }

    private View.OnClickListener clickSkype() {
        return new OnInternetButtonClickListener(mActivity) {
            @Override
            public void onButtonClicked(View v) {
                Uri skypeUri = Uri.parse("skype:tuibagang88?chat");
                Intent myIntent = new Intent(Intent.ACTION_VIEW, skypeUri);
                myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
            }
        };
    }

    private View.OnClickListener clickGmail() {
        return new OnInternetButtonClickListener(mActivity) {
            @Override
            public void onButtonClicked(View v) {
                Intent send = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("contact@tui3gang.vn ");
                Uri uri = Uri.parse(uriText);
                send.setData(uri);
                startActivity(Intent.createChooser(send, "Send mail..."));
            }
        };
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) getActivity()).setToolbarTitle("");
    }

    public  boolean checkPermissionCallPhone(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    PermissionUtil.MY_PERMISSION_READ_PHONE);
            return false;
        }
        return true;
    }
}
