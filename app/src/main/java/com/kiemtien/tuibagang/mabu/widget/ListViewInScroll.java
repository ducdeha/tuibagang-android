package com.kiemtien.tuibagang.mabu.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * DucNT: Xử lý multitouch
 * Khi đặt list này trong scroll view thì chạm vào list sẽ ko tính sự kiện chạm vào Scrollview
 * Khi có viewpager bên trong item của list thì vuốt ngang viewpager tốt hơn, giảm tỉ lệ vuốt viewpager mà ăn vào sự kiện touch list
 */
public class ListViewInScroll extends ListView {
	private boolean mCanSwipe = true;

	public ListViewInScroll(Context context) {
		super(context);
	}

	public ListViewInScroll(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ListViewInScroll(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setCanSwipeList(boolean canSwipe) {
		mCanSwipe = canSwipe;
	}

	float lastX, lastY;
	boolean isConsume = false;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mCanSwipe) {
			int action = event.getActionMasked();
			getParent().requestDisallowInterceptTouchEvent(true);

			if (isConsume) {
				if (action == MotionEvent.ACTION_UP) {
					getParent().requestDisallowInterceptTouchEvent(false);
					isConsume = false;
				}
				return super.onTouchEvent(event);
			}

			switch (action) {
			case MotionEvent.ACTION_DOWN:
				lastX = event.getAxisValue(MotionEvent.AXIS_X);
				lastY = event.getAxisValue(MotionEvent.AXIS_Y);
				break;
			case MotionEvent.ACTION_MOVE:
				float deltaX = event.getAxisValue(MotionEvent.AXIS_X) - lastX;
				float deltaY = event.getAxisValue(MotionEvent.AXIS_Y) - lastY;
				lastX = event.getAxisValue(MotionEvent.AXIS_X);
				lastY = event.getAxisValue(MotionEvent.AXIS_Y);

				if (Math.abs(deltaY) > Math.abs(deltaX)) {
					// Vertical Scroll
					if (computeVerticalScrollOffset() == 0 && deltaY > 0
							&& mCanSwipe) {
						getParent().requestDisallowInterceptTouchEvent(false);
						return true;
					} else {
						getParent().requestDisallowInterceptTouchEvent(true);
						isConsume = true;
					}
				} else {
					return true;
				}
				break;
			case MotionEvent.ACTION_UP:
				lastX = 0;
				lastY = 0;
				getParent().requestDisallowInterceptTouchEvent(false);
				isConsume = false;
			}

			return super.onTouchEvent(event);
		} else {
			return false;
		}
	}
}
