package com.kiemtien.tuibagang.mabu.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kiemtien.tuibagang.mabu.utils.OnSingleClickListener;
import com.kiemtien.tuibagang.mabu.widget.MabuMultiStateView;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public abstract class BaseLoadDataFragment extends BaseFragment {
    protected MabuMultiStateView mMainView;
    public Activity mActivity;
    public Button mBtnFindFriend;
    public View mErrorView, mNoDataView, mContentView, mLoadingView;

    /**
     * @return R.layout.xxx
     */
    public abstract int getLayout();

    public abstract void setupView(View parentView);

    public abstract HashMap<String, String> getParams();

    public abstract String getURL();

    public abstract DataResponse getCallback();

    public abstract ArrayList<?> getArrData();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMainView = (MabuMultiStateView) inflater.inflate(getLayout(), container, false);
        return mMainView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView(view);
        mErrorView = mMainView.getChildByState(MabuMultiStateView.ViewState.ERROR);
        mNoDataView = mMainView.getChildByState(MabuMultiStateView.ViewState.EMPTY);
        mContentView = mMainView.getChildByState(MabuMultiStateView.ViewState.CONTENT);
        mLoadingView = mMainView.getChildByState(MabuMultiStateView.ViewState.LOADING);
        mNoDataView.setOnClickListener(errorViewListener());
        mErrorView.setOnClickListener(errorViewListener());
//        loadData(false);
    }

    private View.OnClickListener errorViewListener() {
        return new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                resetOffset();
                loadData(false);
            }
        };
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    protected void loadData(final boolean isLoadMore) {
        if (!isLoadMore) {
            resetOffset();
            if (mMainView != null)
                mMainView.setState(MabuMultiStateView.ViewState.LOADING);
        }

        DataResponse callback = new DataResponse(getContext()) {

            @Override
            public void onResponse(final Response response) throws IOException {
                super.onResponse(response);
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!response.isSuccessful()) {
                            mMainView.setState(MabuMultiStateView.ViewState.ERROR);
                            viewStateCallback(MabuMultiStateView.ViewState.ERROR);
                        }
                    }
                });
            }

            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                getCallback().onRealSuccess(response);
                if (isAdded()) {

                    if (getArrData().size() > 0) {
                        if (mMainView != null) {
                            mMainView.setState(MabuMultiStateView.ViewState.CONTENT);
                            viewStateCallback(MabuMultiStateView.ViewState.CONTENT);
                        }
                    } else {
                        if (mMainView != null && !isLoadMore) {
                            mMainView.setState(MabuMultiStateView.ViewState.EMPTY);
                            viewStateCallback(MabuMultiStateView.ViewState.EMPTY);
                        }
                    }

                }
            }

            @Override
            public void onFailure(Request request, IOException e) {
                super.onFailure(request, e);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mMainView != null) {
                            mMainView.setState(MabuMultiStateView.ViewState.ERROR);
                            viewStateCallback(MabuMultiStateView.ViewState.ERROR);
                        }
                    }
                });
            }
        };

        if (mActivity != null) {
            BaseRestClient.get(getContext(), getURL(), getParams(), callback, false);
        }
    }

    protected abstract void resetOffset();

    public void viewStateCallback(MabuMultiStateView.ViewState state) {

    }
}
