package com.kiemtien.tuibagang.mabu.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateUtilities {
    // format string.
    public static final String DATE_FORMAT_YYYYMMDD_HH_mm = "yyyy/MM/dd HH:mm";
    public static final String DATE_FORMAT_HH_mm = "HH:mm";

    // constants
    private static final int MILLISECONDS_IN_DAY = 1000 * 60 * 60 * 24;
    private static final int MILLISECONDS_IN_HOUR = 1000 * 60 * 60;
    private static final int MILLISECONDS_IN_TWO_HOUR = 1000 * 60 * 60 * 2;
    private static final int MILLISECONDS_IN_MINUTES = 1000 * 60;

    public static final String DATE_FORMAT_YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_YYYYMMDD_HHMM = "yyyy-MM-dd HH:mm";
    public static final String DATE_FORMAT_HHMM = "HH:mm";
    public static final String DATE_FORMAT_YYYYMMDD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
    public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";



    public static final String DATE_FORMAT_YYYYMMDD_JP = "yyyy年MM月dd日";


    /**
     * @param mili
     * @return day is yesterday
     * @author DucNT
     */
    public static boolean isYesterday(long mili) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(mili);
        Calendar yesterday = Calendar.getInstance();
        yesterday.setTime(new Date());
        yesterday.add(Calendar.DAY_OF_YEAR, -1);

        if (cal.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR)
                && yesterday.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return true;
        } else return false;
    }

    /**
     * getCurrentDate
     *
     * @return current date with format , else blank
     */
    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD_HHMMSS);
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    /**
     * getFirstDateOfWeek
     *
     * @return the first date of the week, else blank
     * @author LinhTH
     */
    public static String getFirstDateOfWeek(String sDate, String format) {
        String retDate = "";
        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
            retDate = convertCalendarToString(calendar, format);

        }

        return retDate;
    }

    /**
     * getLastDateOfWeek
     *
     * @return the last date of the week, else blank
     * @author LinhTH
     */
    public static String getLastDateOfWeek(String sDate, String format) {
        String retDate = "";
        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + 6);
            retDate = convertCalendarToString(calendar, format);
        }

        return retDate;

    }

    /**
     * getFirstDateOfMonth
     *
     * @return the first date of the Month, else blank
     * @author LinhTH
     */
    public static String getFirstDateOfMonth(String sDate, String format) {
        String retDate = "";

        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            // set first day for this calendar
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            retDate = convertCalendarToString(calendar, format);
        }

        return retDate;

    }

    /**
     * @return the last date of the Month, else blank
     * @author LinhTH
     */
    public static String getLastDateOfMonth(String sDate, String format) {
        String retDate = "";

        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            // get number day in this month
            int iNumberDayInMonth = calendar
                    .getActualMaximum(Calendar.DAY_OF_MONTH);

            // set first day for this calendar
            calendar.set(Calendar.DAY_OF_MONTH, iNumberDayInMonth);

            retDate = convertCalendarToString(calendar, format);
        }

        return retDate;

    }

    /**
     * @return the number of day of the Month, else 0
     * @author LinhTH
     */
    public static int getNumberDayInMonth(String sDate, String format) {
        int iNumberDayInMonth = 0;

        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            // get number day in this month
            iNumberDayInMonth = calendar
                    .getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        return iNumberDayInMonth;

    }

    /**
     * convertStringToCalendar
     *
     * @param format
     * @return Calendar object from string and format date, else null
     * @author LinhTH
     */
    public static Calendar convertStringToCalendar(String sDate, String format) {
        Calendar retCalendar = null;

        // get Date.
        Date date = dateFromString(sDate, format);

        // check if convert string to date successfully.
        if (date != null) {
            // get calendar.
            retCalendar = Calendar.getInstance();

            // set date for this calendar
            retCalendar.setTime(date);
        }

        return retCalendar;

    }

    /**
     * convertCalendarToString
     *
     * @param format
     * @return String date from Calendar.
     * @author LinhTH
     */
    public static String convertCalendarToString(Calendar calendar,
                                                 String format) {
        String retStringDate = "";

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        // check if calendar is'nt null
        if (calendar != null) {
            retStringDate = sdf.format(calendar.getTime());
        }

        return retStringDate;

    }

    /**
     * getNextDateOfDate
     *
     * @param sDate
     * @param format
     * @return next date of given date with format, blank if don't get
     * @author LinhTH
     */
    public static String getNextDateOfDate(String sDate, String format) {
        String retStringDate = "";

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            retStringDate = convertCalendarToString(calendar, format);
        }

        return retStringDate;

    }

    /**
     * getPrevDateOfDate
     *
     * @param sDate
     * @param format
     * @return previous date of given date with format, blank if don't get
     * @author LinhTH
     */
    public static String getPrevDateOfDate(String sDate, String format) {
        String retStringDate = "";

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            retStringDate = convertCalendarToString(calendar, format);
        }

        return retStringDate;

    }

    /**
     * getDateAfterAddDay
     *
     * @param sDate
     * @param addDay
     * @param format
     * @return
     * @author LinhTH
     */
    public static String getDateAfterAddDay(String sDate, int addDay,
                                            String format) {
        String retStringDate = "";

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.add(Calendar.DAY_OF_MONTH, addDay);
            retStringDate = convertCalendarToString(calendar, format);
        }

        return retStringDate;

    }

    /**
     * getDateAfterAddMonth
     *
     * @param sDate
     * @param format
     * @return
     * @author LinhTH
     */
    public static String getDateAfterAddMonth(String sDate, int addMonth,
                                              String format) {
        String retStringDate = "";

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {
            calendar.add(Calendar.MONTH, addMonth);
            retStringDate = convertCalendarToString(calendar, format);
        }

        return retStringDate;

    }

    /**
     * getListDateBetweenTwoDate
     *
     * @param startDate
     * @param endDate
     * @param format
     * @return list date between two date.(this function is only correct if
     * format is yyyy/MM/dd)
     * @author LinhTH
     */
    public static List<String> getListDateBetweenTwoDate(String startDate,
                                                         String endDate, String format) {
        List<String> retStringDateList = new ArrayList<String>();

        // check if startDate > endDate
        if (startDate.compareTo(endDate) > 0) {
            return retStringDateList;
        }

        // add first date to list
        retStringDateList.add(startDate);

        while (true) {
            // get next date of start date.
            startDate = getNextDateOfDate(startDate, format);

            // check if start date == ""
            if (startDate == "") {
                break;
            }

            // check if startDate > endDate
            if (startDate.compareTo(endDate) > 0) {
                break;
            }

            // add date to list.
            retStringDateList.add(startDate);

        }

        return retStringDateList;

    }

    /**
     * checkStringIsDate
     *
     * @param date
     * @param format
     * @return true if this String is Date type ,else false.
     * @author LinhTH
     */
    public static boolean checkStringIsDate(String date, String format) {
        boolean isDate = false;
        Calendar calendar = convertStringToCalendar(date, format);
        // check if calendar is'nt null
        if (calendar != null) {
            isDate = true;
        }

        return isDate;
    }

    /**
     * @param sDate
     * @param format
     * @return int day of week,else -1
     * @author LinhTH
     */
    public static int getDayOfWeek(String sDate, String format) {
        int retDayOfWeek = -1;

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(sDate, format);

        // check if calendar is'nt null
        if (calendar != null) {

            retDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        }

        return retDayOfWeek;

    }

    /**
     * @return int day of month,else -1
     * @author LinhTH
     */
    public static int getDayOfMonth(String pDate, String pFormat) {
        int retDayOfWeek = -1;

        // get calendar from string with format.
        Calendar calendar = convertStringToCalendar(pDate, pFormat);

        // check if calendar is'nt null
        if (calendar != null) {

            retDayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);
        }

        return retDayOfWeek;

    }

    /**
     * @param pDate
     * @param pCurrentFormat
     * @param pNewFormat
     * @return string date with new format, else blank
     * @author LinhTH
     */
    public static String convertFormatDate(String pDate, String pCurrentFormat,
                                           String pNewFormat) {
        String sRetDate = "";

        // get calendar with current format.
        Calendar calendar = convertStringToCalendar(pDate, pCurrentFormat);

        // check if calendar is'nt null
        if (calendar != null) {
            sRetDate = convertCalendarToString(calendar, pNewFormat);
        }

        return sRetDate;
    }

    /**
     * getDiffBetweenDate
     *
     * @param format
     * @return number days between two date, ic_notification negative int if startDate >
     * endDate, else 0 (null) if don't get
     * @author LinhTH
     */
    public static int getDiffBetweenDate(String pStartDate, String pEndDate,
                                         String format) {

        // return number of day
        int iDiffDays = 0;

        // get calendar with current format.
        Calendar startCalendar = convertStringToCalendar(pStartDate, format);
        Calendar endCalendar = convertStringToCalendar(pEndDate, format);

        // check if calendar is != null
        if (startCalendar != null && endCalendar != null) {
            try {
                Date dStartDate = startCalendar.getTime();
                Date dEndDate = endCalendar.getTime();

                iDiffDays = (int) ((dEndDate.getTime() - dStartDate.getTime()) / MILLISECONDS_IN_DAY);
            } catch (IllegalArgumentException e) {
                return 0;
            }
        } else {
            return 0;
        }

        return iDiffDays;

    }

    /**
     * getDiffHoursBetweenDate
     *
     * @param format
     * @return number hour between two date, ic_notification negative int if startDate >
     * endDate, else 0 (null) if don't get
     * @author TUAN
     */
    public static int getDiffHoursBetweenDate(String pStartDate,
                                              String pEndDate, String format) {

        // return number of day
        int iDiffHours = 0;

        // get calendar with current format.
        Calendar startCalendar = convertStringToCalendar(pStartDate, format);
        Calendar endCalendar = convertStringToCalendar(pEndDate, format);

        // check if calendar is != null
        if (startCalendar != null && endCalendar != null) {
            try {
                Date dStartDate = startCalendar.getTime();
                Date dEndDate = endCalendar.getTime();
                iDiffHours = (int) ((dEndDate.getTime() - dStartDate.getTime()) / MILLISECONDS_IN_HOUR);
                // Log.d("TUAN BEAN", "EndDAte: "+dEndDate.getTime());
                // Log.d("TUAN BEAN", "StartDAte: "+dStartDate.getTime());
            } catch (IllegalArgumentException e) {
                return 0;
            }
        } else {
            return 0;
        }

        return iDiffHours;

    }

    /**
     * @param pStartDate
     * @param pEndDate
     * @param format
     * @return number hour between two date, ic_notification negative int if startDate >
     * endDate, else 0 (null) if don't get
     */
    public static int getDiffTimeBetweenDateTime(String pStartDate,
                                                 String pEndDate, String format) {

        // return number of day
        int iDiffMinutes = 0;

        // get calendar with current format.
        Calendar startCalendar = convertStringToCalendar(pStartDate, format);
        Calendar endCalendar = convertStringToCalendar(pEndDate, format);

        // check if calendar is != null
        if (startCalendar != null && endCalendar != null) {
            try {
                Date dStartDate = startCalendar.getTime();
                Date dEndDate = endCalendar.getTime();
                iDiffMinutes = (int) ((dEndDate.getTime() - dStartDate
                        .getTime()) / MILLISECONDS_IN_MINUTES);

            } catch (IllegalArgumentException e) {
                return 0;
            }
        } else {
            return 0;
        }

        return iDiffMinutes;

    }

    /**
     * getFirstDateOfMonth
     *
     * @return the first date of the Month , else blank
     * @author LinhTH
     */
    public static String getFirstDateOfMonth(String sDate, String formatIn,
                                             String formatOut) {
        String retDate = "";

        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, formatIn);

        // check if calendar is'nt null
        if (calendar != null) {
            // set first day for this calendar
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            retDate = convertCalendarToString(calendar, formatOut);
        }

        return retDate;

    }

    /**
     * @return the last date of the Month, else blank
     * @author LinhTH
     */
    public static String getLastDateOfMonth(String sDate, String formatIn,
                                            String formatOut) {
        String retDate = "";

        // get calendar.
        Calendar calendar = convertStringToCalendar(sDate, formatIn);

        // check if calendar is'nt null
        if (calendar != null) {
            // get number day in this month
            int iNumberDayInMonth = calendar
                    .getActualMaximum(Calendar.DAY_OF_MONTH);

            // set last day for this calendar
            calendar.set(Calendar.DAY_OF_MONTH, iNumberDayInMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            retDate = convertCalendarToString(calendar, formatOut);
        }

        return retDate;

    }

    /**
     * @param pStartDate
     * @param pEndDate
     * @return days = pEndDate - pStartDate
     * @author VAdaihiep
     */
    public static int substractsDateByDay(Date pStartDate, Date pEndDate) {
        int iDiffDays = 0;
        try {
            iDiffDays = (int) ((pEndDate.getTime() - pStartDate.getTime()) / MILLISECONDS_IN_DAY);
        } catch (IllegalArgumentException e) {
            return 0;
        }
        return iDiffDays;
    }

    /**
     * @param rawBirthday must format like DateUtilities.DATE_FORMAT_YYYYMMDD
     * @return Date from birthday string.
     * @author VAdaihiep
     */
    public static Date dateFromString(String rawBirthday) {
        try {
            return dateFromString(rawBirthday, DATE_FORMAT_YYYYMMDD);
        } catch (Exception e) {
            return new Date();
        }

    }

    public static Date dateFromString(String dateIn, String format) {
        if (dateIn == null || dateIn.equals("null")) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format,
                Locale.getDefault());
        try {
            return simpleDateFormat.parse(dateIn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Format lai cho date
     *
     * @param dateIn    string hien thi date hien tai
     * @param formatIn  dinh dang cho dateIn
     * @param formatOut dinh dang moi
     * @return String theo dinh dang moi
     * @author VAdaihiep
     */
    public static String convertDate(String dateIn, String formatIn,
                                     String formatOut) {
        if (dateIn == null || dateIn.equals("null") || dateIn.equals("")) {
            return "";
        }
        Date date = dateFromString(dateIn, formatIn);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatOut,
                Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    /**
     * In ra string date theo dinh dang mong muon
     *
     * @param dateIn Date input
     * @param format format mong muon
     * @return String theo format
     * @author VAdaihiep
     */
    public static String dateToString(Date dateIn, String format) {
        if (dateIn == null || dateIn.equals("null") || dateIn.equals("")) {
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format,
                Locale.getDefault());

        return simpleDateFormat.format(dateIn);
    }


}
