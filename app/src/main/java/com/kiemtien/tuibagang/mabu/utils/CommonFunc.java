package com.kiemtien.tuibagang.mabu.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kiemtien.tuibagang.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

public class CommonFunc {
    private static ProgressDialog mProgressDialog;
    private static AlertDialog mAlertDialog;

    public static void showDialogOneButton(Context context, String title, String message, DialogInterface.OnClickListener clickOK) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), clickOK);
        pShowDialog(builder);
    }

    public static void showDialogTwoButton(Context context, String title, String message, DialogInterface.OnClickListener clickOK) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), clickOK).setNegativeButton(context.getString(R.string.cancel), null);
        pShowDialog(builder);
    }

    public static void showDialogTwoButtonTwoClicklistener(Context context, String title, String message, DialogInterface.OnClickListener clickOK, DialogInterface.OnClickListener clickCancel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), clickOK).setNegativeButton(context.getString(R.string.cancel), clickCancel);
        pShowDialog(builder);
    }

    public static void showDialogTwoButtonTwoClicklistener(Context context, String title, String message, String okString, DialogInterface.OnClickListener clickOK, DialogInterface.OnClickListener clickCancel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(okString, clickOK).setNegativeButton(context.getString(R.string.cancel), clickCancel);
        pShowDialog(builder);
    }

    public static void showDialogOneButton(Context context, String title, String message) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), null);
        pShowDialog(builder);
    }

    public static void showDialogTwoButtonNotitle(Context context, String message, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), clickOk).setNegativeButton(context.getString(R.string.cancel), clickCancel);


        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        } else if (mAlertDialog != null) {
            mAlertDialog = null;
        }

        mAlertDialog = builder.create();
        mAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mAlertDialog.show();

    }

    private static void pShowDialog(AlertDialog.Builder builder) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        } else if (mAlertDialog != null) {
            mAlertDialog = null;
        }
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static String getDeviceName() {
        String deviceName = "";
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        if (myDevice != null)
            deviceName = myDevice.getName();
        return deviceName;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Coult not get package name: " + e);
        }
    }

    public static String encodeTextUTF8(String text) {
        String textEncoded = "";
        try {
            textEncoded = URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return textEncoded;
    }

    public static String decodeTextUTF8(String text) {
        if (text == null) text = "";
        String textDecoded = "";
        try {
            textDecoded = (URLDecoder.decode(text, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return textDecoded;
    }

    public static void showSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    public static void showSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED);
    }

    public static void hideSoftKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static void showLoadingView(Context context) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
        }
        mProgressDialog.setMessage(context.getString(R.string.loading));
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public static void hideLoadingView() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    public static String convertArrayIntToString(ArrayList<Integer> list) {
        if (list == null) return "";
        if (list.size() > 0) {
            StringBuffer categories = new StringBuffer();
            for (int i = 0; i < list.size(); i++) {
                int category = list.get(i);
                categories.append(category + ",");
            }
            return categories.toString().substring(0, categories.length() - 1);
        }
        return "";
    }

    public static boolean pIsGPSEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            pShowAlertMessageNoGps(context);
            return false;
        }
        return true;
    }

    private static void pShowAlertMessageNoGps(final Context context) {
        showDialogTwoButtonTwoClicklistener(context, "", "Your GPS seems to be disabled, do you want to enable it?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }, null);
    }

    public static Bitmap rotateBitmap(Bitmap source, int angel) {
        Bitmap result = null;
        Matrix matrix = new Matrix();
        if ((angel == ExifInterface.ORIENTATION_ROTATE_180)) {
            matrix.postRotate(180);
            result = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } else if (angel == ExifInterface.ORIENTATION_ROTATE_90) {
            matrix.postRotate(90);
            result = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } else if (angel == ExifInterface.ORIENTATION_ROTATE_270) {
            matrix.postRotate(270);
            result = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } else {
            return source;
        }
        return result;
    }

    public static int getOrientation(String path) {
        int exifOrientation;
        ExifInterface exif;
        try {
            exif = new ExifInterface(path);
            exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, -1);
        } catch (IOException e1) {
            return -1;
        }
        return exifOrientation;
    }

    public static float convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static void destroy() {
        mProgressDialog = null;
        mAlertDialog = null;
    }


    /**
     * DucNT: Lay url khi up ảnh bằng google photo với album online
     *
     * @param context
     * @param uri
     * @return
     */
    public static String getImageUrlWithAuthority(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }


    public static void loadCircleImageFromUrl(final Context context, final ImageView imageView, String url) {
        if (imageView == null) return;
        imageView.setImageDrawable(null);
        Glide.with(context).load(url).asBitmap().centerCrop().override(100, 100).into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    public static void loadImageFromUri(Context context, ImageView imageView, String uri) {
        imageView.setImageURI(null);
        Glide.with(context)
                .load(uri)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .crossFade()
                .into(imageView);
    }

    public static String getUuid(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tManager.getDeviceId() == null)
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return tManager.getDeviceId();
    }

    public static boolean isAppInstalled(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static String formatedCoin(long coin) {
        try {
            String strCoin = String.format("%,d", coin);
            return strCoin.replaceAll(",", ".");
        } catch (Exception e) {
            return String.valueOf(coin);
        }
    }

    public static String formatedCoin(String coin) {
        try {
            long lCoin = Long.parseLong(coin);
            String strCoin = String.format("%,d", lCoin);
            return strCoin.replaceAll(",", ".");
        } catch (Exception e) {
            return coin;
        }
    }

    public static String getNetworkInfomation(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getSimOperatorName();
    }

    public static String getImei(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    public static boolean isWifiEnable(Context context) {
        ConnectivityManager conMan = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
    }


}
