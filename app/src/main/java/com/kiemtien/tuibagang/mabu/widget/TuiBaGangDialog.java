package com.kiemtien.tuibagang.mabu.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 5/4/16.
 */
public class TuiBaGangDialog extends Dialog {
    private Context mContext;
    private ObjLogin mObjLogin;
     EditText mEditText;

    public TuiBaGangDialog(Context context, String title, String content, OnPosibleClickListener listener) {
        super(context);
        mContext = context;
        mObjLogin = SharePreferences.getUserInfo(mContext);
        initView(title,content,listener);
    }


    private void initView(String title, String content, final OnPosibleClickListener listener) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.answer_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(true);
        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvContent = (TextView) findViewById(R.id.content);
        TextView okBtn = (TextView) findViewById(R.id.ok);
        TextView cancelBtn = (TextView) findViewById(R.id.back);
        mEditText = (EditText) findViewById(R.id.input_box);

        //DucNT: fillData:
        tvTitle.setText(title);
        tvContent.setText(content);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(mEditText.getText().toString());
                mEditText.setText("");
                mEditText.clearFocus();
                dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
    public void setHindEditText(String text){
        mEditText.setHint(text);
    }

    public interface OnPosibleClickListener{
        void OnClick(String edtText);
    }
}
