package com.kiemtien.tuibagang.mabu.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import com.kiemtien.tuibagang.login.ObjLogin;


public class SharePreferences {
    private final static String PREF_DEVICE_TOKEN_KEY = "device_token_preference";
    //GCM:
    public static final String KEY_PREF_REGISTER_GCM = "key_register_gcm";
    public static final String KEY_PREF_APP_VERSION = "key_app_version";
    public final static String PREF_KEY = "tuibagang_preference";
    // key save user info
    public static final String KEY_PREF_USER_INFO = "key_pref_user_info";
    public static final String KEY_PREF_FACEBOOK_ACCESS_TOKEN = "facebook_access_token";

    public static ObjLogin getUserInfo(Context pContex) {
        if (pContex.getSharedPreferences(PREF_KEY, 0) != null) {
            String info =  pContex.getSharedPreferences(PREF_KEY, 0)
                    .getString(KEY_PREF_USER_INFO, "");
            Gson gson = new Gson();
            ObjLogin objLogin = gson.fromJson(info,ObjLogin.class);
            return  objLogin;
        } else {
            return null;
        }

    }


    // get string preference
    public static String getStringPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_KEY, 0)
                    .getString(strKey, "");
        } else {
            return null;
        }

    }

    public static int getIntPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_KEY, 0)
                    .getInt(strKey, -1);
        } else {
            return -1;
        }

    }

    public static boolean getBoolPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_KEY, 0)
                    .getBoolean(strKey, false);
        } else {
            // default is true
            return true;
        }

    }

    public static float getFloatPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_KEY, 0)
                    .getFloat(strKey, -1);
        } else {
            return -1;
        }

    }

    public static boolean saveStringPreference(Context pContext, String strKey,
                                               String strValue) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_KEY, 0)
                    .edit();
            e.putString(strKey, strValue);
            e.commit();
            return true;
        }
        return false;
    }

    public static boolean saveIntPreference(Context pContext, String strKey,
                                            int iValue) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_KEY, 0)
                    .edit();
            e.putInt(strKey, iValue);
            e.commit();
            return true;
        }
        return false;
    }

    public static boolean saveBooleanPreference(Context pContext,
                                                String strKey, boolean bValue) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_KEY, 0)
                    .edit();
            e.putBoolean(strKey, bValue);
            e.commit();
            return true;
        }
        return false;
    }

    public static boolean saveFloatPreference(Context pContext, String strKey,
                                              float fValue) {
        if (pContext.getSharedPreferences(PREF_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_KEY, 0)
                    .edit();
            e.putFloat(strKey, fValue);
            e.commit();
            return true;
        }
        return false;
    }


    // get string preference
    public static String getDeviceTokenPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0)
                    .getString(strKey, "");
        } else {
            return null;
        }

    }

    public static boolean saveDeviceTokenPreference(Context pContext, String strKey,
                                                    String strValue) {
        if (pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0)
                    .edit();
            e.putString(strKey, strValue);
            e.commit();
            return true;
        }
        return false;
    }

    public static int getAppVersionPreference(Context pContext, String strKey) {
        if (pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0) != null) {
            return pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0)
                    .getInt(strKey, -1);
        } else {
            return -1;
        }

    }


    public static boolean saveAppVersionPreference(Context pContext, String strKey,
                                                   int iValue) {
        if (pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0) != null) {

            SharedPreferences.Editor e = pContext.getSharedPreferences(PREF_DEVICE_TOKEN_KEY, 0)
                    .edit();
            e.putInt(strKey, iValue);
            e.commit();
            return true;
        }
        return false;
    }
}
