package com.kiemtien.tuibagang.mabu.utils;

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;

import com.google.android.gms.location.GeofenceStatusCodes;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Temperature {
    public static String readSystemFile(String pSystemFile) throws Exception {
        if (new File(pSystemFile).exists()) {
            try {
                InputStream is;
                Process process = Runtime.getRuntime().exec(su());
                DataOutputStream localDataOutputStream = new DataOutputStream(process.getOutputStream());
                localDataOutputStream.writeBytes("/system/bin/cat " + pSystemFile + "\n");
                localDataOutputStream.flush();
                localDataOutputStream.writeBytes("exit\n");
                localDataOutputStream.flush();
                if (process.waitFor() == 0) {
                    is = process.getInputStream();
                } else {
                    is = process.getErrorStream();
                }
                BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(is), AccessibilityNodeInfoCompat.ACTION_PREVIOUS_HTML_ELEMENT);
                StringBuilder total = new StringBuilder();
                while (true) {
                    String line = localBufferedReader.readLine();
                    if (line != null) {
                        total.append(line);
                    } else {
                        localBufferedReader.close();
                        return total.toString();
                    }
                }
            } catch (Exception e) {
                throw new Exception(e);
            }
        }
        throw new Exception();
    }

    public static List<String> readFileByLine(String pFile) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(pFile));
            List<String> arrayList = new ArrayList();
            while (true) {
                String line = br.readLine();
                if (line != null) {
                    arrayList.add(line);
                } else {
                    br.close();
                    return arrayList;
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    private static String su() {
        String str1 = "/system/bin/su";
        String str2 = "/system/xbin/su";
        if (new File(str1).exists()) {
            return str1;
        }
        if (new File(str2).exists()) {
            return str2;
        }
        return null;
    }

    public static Integer getTemp() {
        String[] cputemp = new String[]{"/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp", "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp", "/sys/class/thermal/thermal_zone1/Temperature", "/sys/class/i2c-adapter/i2c-4/4-004c/temperature", "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature", "/sys/devices/platform/tegra_tmon/temp1_input", "/sys/kernel/debug/tegra_thermal/temp_tj", "/sys/devices/platform/s5p-tmu/temperature", "/sys/class/thermal/thermal_zone0/Temperature", "/sys/devices/virtual/thermal/thermal_zone0/Temperature", "/sys/class/hwmon/hwmon0/device/temp1_input", "/sys/devices/virtual/thermal/therm/sys/class/hwmon/hwmon0/device/temp1_inputal_zone1/Temperature", "/sys/devices/platform/s5p-tmu/curr_temp", "/sys/devices/virtual/thermal/thermal_zone0/temp", "/sys/class/thermal/thermal_zone1/temp"};
//        List<String> arrLinks = Arrays.asList(cputemp);
        List<String> arrLinks = new LinkedList<String>(Arrays.asList(cputemp));

        for (int i = 0 ; i < 30 ; i ++){
            arrLinks.add("/sys/class/hwmon/hwmon" + i + "/temp1_input");
            arrLinks.add("/sys/class/thermal/thermal_zone" + i + "/temp");
        }

        for (String str : arrLinks) {
            try {
                List<String> res = readFileByLine(str);
                if (res.size() == 0) {
                    continue;
                } else {
                    int j = 0;
                    while (j < res.size()) {
                        try {
                            Integer value = Integer.valueOf((String) res.get(j));
                            if (value.intValue() < 100) {
                                return value;
                            }
                            if (value.intValue() > GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE && value.intValue() < 100000) {
                                return Integer.valueOf(value.intValue() / GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                            }
                            j++;
                        } catch (Exception e) {
                        }
                    }
                    continue;
                }
            } catch (Exception e2) {
            }
        }
        return 0;
    }
}
