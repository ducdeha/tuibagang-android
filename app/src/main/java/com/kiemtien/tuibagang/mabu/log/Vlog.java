package com.kiemtien.tuibagang.mabu.log;

import android.util.Log;


/**
 * Created by DucNT 19/04/2015
 */
public class Vlog {
    public static final String DEFAULT_TAG = "Deha";
    public static final boolean IS_DEBUG= true;

    public static void d(String tag, String msg) {
        if (IS_DEBUG) {
            Log.d(tag, msg);
        }
    }

    /**
     * Log d with default tag
     *
     * @param msg
     */
    public static void d(String msg) {
        d(DEFAULT_TAG, msg);
    }

    public static void i(String tag, String msg) {
        if (IS_DEBUG) {
            Log.i(tag, msg);
        }
    }

    /**
     * Log i with default tag
     *
     * @param msg
     */
    public static void i(String msg) {
        i(DEFAULT_TAG, msg);
    }

    public static void e(String tag, String msg) {
        if (IS_DEBUG) {
            Log.e(tag, msg);
        }
    }

    /**
     * Log e with default tag
     *
     * @param msg
     */
    public static void e(String msg) {
        e(DEFAULT_TAG, msg);
    }

    public static void w(String tag, String msg) {
        if (IS_DEBUG) {
            Log.w(tag, msg);
        }
    }
    /**
     * Log w with default tag
     *
     * @param msg
     */
    public static void w(String msg) {
        w(DEFAULT_TAG, msg);
    }
}
