package com.kiemtien.tuibagang.mabu.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.kiemtien.tuibagang.R;


/**
 * DucNT: 2015/12/30
 * Check internet when pressing on a button
 */
public abstract class OnInternetButtonClickListener implements View.OnClickListener {
    private Context mContext;

    public OnInternetButtonClickListener(Context context) {
        mContext = context;
    }

    public OnInternetButtonClickListener(Activity context) {
        mContext = context;
    }

    @Override
    public final void onClick(View v) {
        if (CommonFunc.isConnectingToInternet(mContext))
            onButtonClicked(v);
        else
            CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.title_error), mContext.getString(R.string.no_network));
    }

    public abstract void onButtonClicked(View v);
}
