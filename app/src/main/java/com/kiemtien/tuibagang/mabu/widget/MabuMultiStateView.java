package com.kiemtien.tuibagang.mabu.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

import com.kiemtien.tuibagang.R;

/**
 * Hiển thị các trạng thái loading, error, empty, loading khi load data
 */
public class MabuMultiStateView extends ViewFlipper {

    public enum ViewState {
        LOADING,
        CONTENT,
        ERROR,
        EMPTY
    }

    private ViewState mCurrentState;

    public MabuMultiStateView(Context context) {
        super(context);
        init();
    }

    public MabuMultiStateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        // Animation default
        Animation fadeIn = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_in);
        Animation fadeOut = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_out);
        setInAnimation(fadeIn);
        setOutAnimation(fadeOut);
    }

    /**
     * @param state
     */
    public void setState(ViewState state) {
        mCurrentState = state;
        int childId = 0;
        switch (mCurrentState) {
            case LOADING:
                childId = R.id.loading_view;
                break;
            case CONTENT:
                childId = R.id.content_view;
                break;
            case ERROR:
                childId = R.id.error_view;
                break;
            case EMPTY:
                childId = R.id.empty_view;
                break;
        }

        int pos = getChildIndex(childId);
        if (pos > -1 && getDisplayedChild() != pos) {

            setDisplayedChild(getChildIndex(childId));
        } /*else {
//            Vlog.e("Not have child " + state);
        }*/

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mCurrentState != null) {
            setState(mCurrentState);
        }
    }

    /**
     * @param childId
     * @return
     */
    public int getChildIndex(int childId) {
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i).getId() == childId) {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param state
     * @return
     */
    public View getChildByState(ViewState state) {
        switch (state) {
            case LOADING:
                return findViewById(R.id.loading_view);
            case CONTENT:
                return findViewById(R.id.content_view);
            case ERROR:
                return findViewById(R.id.error_view);
            case EMPTY:
                return findViewById(R.id.empty_view);
        }
        return null;
    }

    /**
     * @return
     */
    public ViewState getCurrentState() {
        return mCurrentState;
    }

}
