package com.kiemtien.tuibagang.mabu.utils;

import android.os.SystemClock;
import android.view.View;

/**
 * Created by Th3M4sk on 11/26/15.
 */
public abstract class OnSingleClickListener implements View.OnClickListener {
    private static final long MIN_CLICK_INTERVAL = 1000;
    private long mLastClickTime;


    /**
     * Override onSingleClick() instead.
     */
    @Override
    public final void onClick(View v) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;

        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;

        onSingleClick(v);
    }

    /**
     * Override this function to handle clicks.
     * reset() must be called after each click for this function to be called
     * again.
     *
     * @param v
     */
    public abstract void onSingleClick(View v);
}
