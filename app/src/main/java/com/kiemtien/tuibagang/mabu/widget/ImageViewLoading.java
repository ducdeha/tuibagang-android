package com.kiemtien.tuibagang.mabu.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;

import com.kiemtien.tuibagang.R;

/**
 * DucNT: Hiển thị imageview với progress ở giữa
 * Lưu ý: Lớp này cho hiển thị image với kích thước hình vuông
 */
public class ImageViewLoading extends RelativeLayout {

    private ImageView mImageView;
    private ProgressBar mProgressBar;


    public ImageViewLoading(Context context) {
        super(context);
        initView(context);
    }

    public ImageViewLoading(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ImageViewLoading(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {

        inflate(context, R.layout.include_image_view_loading, this);
        mImageView = (ImageView) findViewById(R.id.image_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);

    }

    public void setImageView(String url) {
        if (url.length() == 0) {
            mImageView.setImageResource(R.drawable.ico_tuibagang_header);
            mProgressBar.setVisibility(View.GONE);
            return;
        }
        mImageView.setImageURI(null);
        mProgressBar.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(url)
                .fitCenter()
                .listener(loadImageListener(mProgressBar))
                .into(mImageView);

    }

    public void setImageAsAvatar(String url, final Context context) {
        final int radius = 45;
        if (url.length() == 0) {
            Glide.with(getContext())
                    .load(R.drawable.ico_tuibagang_header).asBitmap()
                    .fitCenter()
                    .into(new BitmapImageViewTarget(mImageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCornerRadius(radius);
                            mImageView.setImageDrawable(circularBitmapDrawable);
                            mProgressBar.setVisibility(View.GONE);
                        }
                    });
            return;
        }
        mImageView.setImageURI(null);
        mProgressBar.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(url).asBitmap()
                .fitCenter()
                .into(new BitmapImageViewTarget(mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCornerRadius(radius);
                        mImageView.setImageDrawable(circularBitmapDrawable);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });

    }

    public void setCircleImg(String url, final Context context) {
        if (url.length() == 0) {
            Glide.with(getContext())
                    .load(R.drawable.ico_tuibagang_header).asBitmap()
                    .fitCenter()
                    .into(new BitmapImageViewTarget(mImageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            mImageView.setImageDrawable(circularBitmapDrawable);
                            mProgressBar.setVisibility(View.GONE);
                        }
                    });
            return;
        }
        mImageView.setImageURI(null);
        mProgressBar.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(url).asBitmap()
                .fitCenter()
                .into(new BitmapImageViewTarget(mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        mImageView.setImageDrawable(circularBitmapDrawable);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });

    }

    public void setImageView(String url, int width, int height) {
        if (url.length() == 0) {
            mImageView.setImageResource(R.drawable.ico_tuibagang_header);
            mProgressBar.setVisibility(View.GONE);
            return;
        }
        mImageView.setImageURI(null);
        mProgressBar.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(url)
                .centerCrop()
                .listener(loadImageListener(mProgressBar)).override(width, height)
                .into(mImageView);

    }

    private RequestListener<? super String, GlideDrawable> loadImageListener(final ProgressBar progressBar) {
        return new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;

            }
        };
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
