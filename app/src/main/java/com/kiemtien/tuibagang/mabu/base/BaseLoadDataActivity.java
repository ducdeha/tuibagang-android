package com.kiemtien.tuibagang.mabu.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.squareup.okhttp.Request;
import com.kiemtien.tuibagang.network.BaseRestClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.mabu.utils.OnSingleClickListener;
import com.kiemtien.tuibagang.network.DataResponse;


public abstract class BaseLoadDataActivity extends AppCompatActivity {
    public static final int STATE_LOADING = 0;
    public static final int STATE_ERROR = 1;
    public static final int STATE_EMPTY = 2;
    public static final int STATE_CONTENT = 3;


    private View mErrorView, mNoDataView, mLoadingView, mContentView;

    /**
     * @return R.layout.xxx
     */
    public abstract int getLayout();

    public abstract void setupView();

    public abstract HashMap<String, String> getParams();

    public abstract String getURL();

    public abstract DataResponse getCallback();

    public abstract ArrayList<?> getArrData();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mLoadingView = findViewById(R.id.loading_view);
        mErrorView = findViewById(R.id.error_view);
        mNoDataView = findViewById(R.id.empty_view);
        mContentView = findViewById(R.id.content_view);
        mErrorView.setOnClickListener(errorViewListener());
        mNoDataView.setOnClickListener(errorViewListener());
        setState(STATE_CONTENT);
        setupView();
    }

    public void setState(int state) {
        switch (state) {
            case STATE_LOADING:
                mLoadingView.setVisibility(View.VISIBLE);
                mErrorView.setVisibility(View.GONE);
                mNoDataView.setVisibility(View.GONE);
                mContentView.setVisibility(View.GONE);
                break;
            case STATE_ERROR:
                mLoadingView.setVisibility(View.GONE);
                mErrorView.setVisibility(View.VISIBLE);
                mContentView.setVisibility(View.GONE);
                mNoDataView.setVisibility(View.GONE);
                break;
            case STATE_EMPTY:
                mLoadingView.setVisibility(View.GONE);
                mErrorView.setVisibility(View.GONE);
                mNoDataView.setVisibility(View.VISIBLE);
                mContentView.setVisibility(View.GONE);
                break;
            case STATE_CONTENT:
                mLoadingView.setVisibility(View.GONE);
                mErrorView.setVisibility(View.GONE);
                mNoDataView.setVisibility(View.GONE);
                mContentView.setVisibility(View.VISIBLE);
                break;
        }

    }


    private View.OnClickListener errorViewListener() {
        return new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                resetOffset();
                loadData(false);
            }
        };
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    protected void loadData(final boolean isLoadMore) {
        if (!isLoadMore) {
            resetOffset();
            setState(STATE_LOADING);
        }

        DataResponse callback = new DataResponse(BaseLoadDataActivity.this) {

            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                getCallback().onRealSuccess(response);
                if (response.length() > 0 && getArrData().size() > 0) {
                   setState(STATE_CONTENT);
                } else if (getArrData() != null && getArrData().size() == 0) {
                    setState(STATE_EMPTY);
                }
            }

            @Override
            public void onFailure(Request request, IOException e) {
                super.onFailure(request, e);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setState(STATE_ERROR);
                    }
                });
            }
        };
        BaseRestClient.get(BaseLoadDataActivity.this, getURL(), getParams(), callback, false);
    }

    protected abstract void resetOffset();

}
