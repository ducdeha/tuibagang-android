package com.kiemtien.tuibagang.mabu.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;


/**
 * Created by TuanBean on 8/17/15.
 */
public abstract class BaseFragment extends Fragment {


    private Handler mHandler;

    @LayoutRes
    protected int getLayoutRes() {
        return 0;
    }

    public String getTitle() {
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getTitle() != null) {
            getActivity().setTitle(getTitle());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) return;
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


    /**
     * Shouldn't override this function...Use {@link #getLayoutRes()}
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = getLayoutRes();
        if (layoutRes != 0) {
            return inflater.inflate(layoutRes, container, false);
        }


        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        DestroyUtils.destroyHandler(mHandler);
    }

    protected void showDialogFail() {

    }

}
