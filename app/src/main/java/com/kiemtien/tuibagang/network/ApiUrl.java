package com.kiemtien.tuibagang.network;

/**
 * Created by Win 8.1 VS8 X64 on 19/04/2016.
 */
public class ApiUrl {
    public static final String BASE_URL = "http://tbg.thanbai69.com/";
//    public static final String BASE_URL = "http://45.32.58.106:3001/";
    public static final String URL_LOGIN = BASE_URL + "login";
    public static final String URL_GET_LIST_APP = BASE_URL + "listapp";
    public static final String URL_INSTALL_APP = BASE_URL + "installapp";
    public static final String URL_GET_LIST_EVENTS = BASE_URL + "getListEvents";
    public static final String URL_GET_LIST_CARD = BASE_URL + "getListCard";
    public static final String URL_GET_CARD_HISTORY = BASE_URL + "getCardHistory";
    public static final String URL_CANCEL_REQUEST = BASE_URL + "cancelRequest";
    public static final String URL_ACTIVITY = BASE_URL + "activity";
    public static final String URL_TOP_RANK = BASE_URL + "topRank";
    public static final String URL_CAU_HOI = BASE_URL + "faq";
    public static final String URL_THONGBAO = BASE_URL + "listNotification";
    public static final String URL_GET_COIN = BASE_URL + "doquestion";
    public static final String URL_EVENT_FEEDBACK = BASE_URL + "EventFeedback";
    public static final String URL_GET_CARD = BASE_URL + "getCard";
    public static final String URL_LIST_CONTACT = BASE_URL + "contact";
    public static final String URL_CHATTING = BASE_URL + "chatting";
    public static final String URL_CLICK_APP = BASE_URL + "clickapp";
    public static final String URL_CHECK_INSTALL = BASE_URL + "checkinstall";
    public static final String URL_SHARE_CODE = BASE_URL + "enterShareCode";
    public static final String URL_SHARE_EVENT = BASE_URL + "sharing";
    public static final String URL_BLOCK = BASE_URL + "block";
    public static final String LOG = BASE_URL + "access_log";

}
