package com.kiemtien.tuibagang.network;

/**
 * Created by Win 8.1 VS8 X64 on 19/04/2016.
 */
public class ApiKey {

    //Login
    public static final String PUBLISHER_SOCIAL_ID = "publisher_social_id";
    public static final String PUBLISHER_EMAIL  =  "publisher_email";
    public static final String PUBLISHER_AVATAR_URL = "publisher_avatar_url";
    public static final String PUBLISHER_FACEBOOK_NAME = "publisher_facebook_name";
    public static final String PUBLISHER_CITY = "publisher_city";
    public static final String BIRTHDAY =  "birthday";
    public static final String PUBLISHER_SEX = "publisher_sex";
    public static final String PUBLISHER_AGE = "publisher_age";
    public static final String PUBLISHER_CAMPAIGN_ID = "campaign_id";
    public static final String DEVICE_ID ="device_id";
    public static final String GCM_ID = "gcm_id";
    public static final String ACCEPT_TOKEN = "accept_token";
    public static final String DETAIL = "message";
    public static final String DEVICE_MODEL = "device_model";
    public static final String DEVICE_MANUFACTORY= "device_factory";
    public static final String DEVICE_VERSION= "device_version";
    public static final String DEVICE_IP= "device_ip";
    public static final String DEVICE_SERI= "device_seri";
    public static final String DEVICE_MAC_ADDRESS= "device_mac_address";
    public static final String EVENT_ID= "event_id";
    public static final String CONTENT= "content";
    public static final String SHARE_CODE= "share_code";
    public static final String NETWORK_PROVIDER= "network_provider";
    public static final String DEVICE_IMEI= "device_imei";
    public static final String IS_WIFI_ENABLED= "is_wifi_enabled";
    public static final String APP_VERSION= "app_version";
    public static final String ACTION= "action";
    public static final String ACCESS_LOG_COIN= "accesslog_coin";

    //Kiem tien
    public static final String OFFSET = "offset";
    public static final String PUBLISHER_USER_ID = "publisher_user_id";
    public static final String MOBILE_CARD_ID = "mobile_card_id";
    public static final String OS = "os";

    //Lich su doi thuong
    public static final String MOBILE_CARD_HISTORY_ID = "mobile_card_history_id";




}
