package com.kiemtien.tuibagang.network;

import android.content.Context;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.kiemtien.tuibagang.mabu.log.Vlog;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.mabu.utils.CommonFunc;

/**
 * Created by DucNT on 19/04/15.
 * Khi bắt đầu dự án thì sv sử dụng form-data để request => dùng lớp này.
 * Tuy nhiên sau đó, sv chuyển lại sử dụng raw data nên tôi khuyến cáo anh em sử dụng lớp BaseRequestClient.
 * Lớp này để lại đề phòng có thể dùng lại sau.
 */
public class RequestClient {
    public static final int TIME_OUT = 90;//in second
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static OkHttpClient mClient = new OkHttpClient();
    public static Request request;
    public static final String METHOD_GET = "get";
    public static final String METHOD_POST = "post";
    public static final String METHOD_PUT = "put";
    public static final String METHOD_DELETE = "delete";

    /**
     * DucNT: Build url từ params
     * Ví dụ params có {"key1":"value1","key2":"value2"}, url:http:xyz.com
     * thì build xong sẽ được full url là: http:xyz.com?key1=value1&key2=value2
     * Sau đó thực hiện method get, không giống như phương thức post ( add params vào được )
     */
    public static void get(Context pContext, String url, HashMap<String, String> params, Callback response) {
        BaseRestClient.addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            StringBuilder urlParams = new StringBuilder(url + "?");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                urlParams.append(key).append("=").append(value).append("&");
            }
            String fullURL = urlParams.toString();
            if (fullURL.length() > 0) {
                fullURL = fullURL.substring(0, fullURL.length() - 1);
            }
            request = new Request.Builder()
                    .url(fullURL)
                    .method(METHOD_GET, null)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
            Vlog.d("Get:" + fullURL);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void get(Context pContext, String url, HashMap<String, String> params, Callback response, boolean isShowLoading) {
        BaseRestClient.addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);

        if (CommonFunc.isConnectingToInternet(pContext)) {
            if (isShowLoading)
                CommonFunc.showLoadingView(pContext);
            StringBuilder urlParams = new StringBuilder(url + "?");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                urlParams.append(key).append("=").append(value).append("&");
            }
            String fullURL = urlParams.toString();
            if (fullURL.length() > 0) {
                fullURL = fullURL.substring(0, fullURL.length() - 1);
            }
            request = new Request.Builder()
                    .url(fullURL)
                    .method(METHOD_GET, null)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
            Vlog.d("Get:" + fullURL);
        } else {
        }
    }

    /**
     * DucNT: Hàm này truyền vào params là JsonObject thì sẽ tốt hơn
     * Nhưng truyền vào kiểu Hashmap để đồng bộ với phương thức get
     * thuận tiện cho việc thay đổi phương thức
     */
    public static void post(Context pContext, String url, HashMap<String, String> params, DataResponse response) {
        BaseRestClient.addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        MultipartBuilder multipart = new MultipartBuilder()
                .type(MultipartBuilder.FORM);
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            multipart.addFormDataPart(key, value);
        }
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            Request.Builder builder = new Request.Builder().url(url).post(multipart.build());
            builder.header("Accept", "application/json");
            request = builder.build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
        Vlog.d("Post:" + url + "?" + params.toString());
    }
}
