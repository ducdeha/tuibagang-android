package com.kiemtien.tuibagang.network;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.kiemtien.tuibagang.login.LoginActivity;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.mabu.utils.CommonFunc;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DucNT on 19/04/2015.
 */
public class DataResponse implements Callback {
    public Context mContext;

    public DataResponse(Context pContext) {
        mContext = pContext;
    }

    @Override
    public void onFailure(Request request, IOException e) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CommonFunc.hideLoadingView();
                CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.title_error), mContext.getString(R.string.msg_error));
            }
        });
    }

    @Override
    public void onResponse(final Response response) throws IOException {
        ((Activity) mContext).runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {

                        CommonFunc.hideLoadingView();
                        if (!response.isSuccessful()) {
                            Log.d("DucNT ", " not success");
                            try {
                                String msg = response.body().string();
                                JSONObject jsonResponse = new JSONObject(msg);
                                int statusCode = response.code();
                                if (statusCode == 404) {
                                    on404();
                                    return;
                                }
                                if (jsonResponse.isNull(ApiKey.DETAIL))
                                    showDialogUpdateError(null);
                                else
                                    showDialogUpdateError(jsonResponse.getString(ApiKey.DETAIL));
                                onNotSuccess();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                        try {
                            onRealSuccess(response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void onRealSuccess(String response) {

    }

    public void onNotSuccess() {

    }

    public void on404() {

    }

    public void showDialogUpdateError(final String error) {
        if (TextUtils.isEmpty(error)) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.nav_thongbao), mContext.getString(R.string.msg_error));
                }
            });
            return;
        }

        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (error.equalsIgnoreCase("Phiên làm việc đã hết hạn, bạn vui lòng đăng nhập lại")) {
                    CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.nav_thongbao), error, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences settings = mContext.getSharedPreferences(SharePreferences.PREF_KEY, Context.MODE_PRIVATE);
                            settings.edit().clear().commit();
                            Intent i = new Intent(mContext, LoginActivity.class);
                            mContext.startActivity(i);
                            ((Activity) mContext).finish();
                        }
                    });
                } else
                    CommonFunc.showDialogOneButton(mContext, mContext.getString(R.string.nav_thongbao), error);
            }
        });
    }

}
