package com.kiemtien.tuibagang.network;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.kiemtien.tuibagang.BuildConfig;
import com.kiemtien.tuibagang.login.LoginActivity;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.log.Vlog;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.kiemtien.tuibagang.R;

import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.squareup.okhttp.Response;

/**
 * Created by DucNT on 19/04/15.
 */
public class BaseRestClient {
    public static final int TIME_OUT = 90;//in second
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static OkHttpClient mClient = new OkHttpClient();
    public static Request request;
    public static final String METHOD_GET = "get";
    public static final String METHOD_POST = "post";
    public static final String METHOD_PUT = "put";
    public static final String METHOD_DELETE = "delete";

    /**
     * DucNT: Build url từ params
     * Ví dụ params có {"key1":"value1","key2":"value2"}, url:http:xyz.com
     * thì build xong sẽ được full url là: http:xyz.com?key1=value1&key2=value2
     * Sau đó thực hiện method get, không giống như phương thức post ( add params vào được )
     */
    public static void get(Context pContext, String url, HashMap<String, String> params, Callback response) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);

        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            StringBuilder urlParams = new StringBuilder(url + "?");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                urlParams.append(key).append("=").append(value).append("&");
            }
            String fullURL = urlParams.toString();
            if (fullURL.length() > 0) {
                fullURL = fullURL.substring(0, fullURL.length() - 1);
            }
            request = new Request.Builder()
                    .url(fullURL)
//                    .method(METHOD_GET, null)
                    .addHeader("Accept", "application/json")
                    .get()
                    .build();
            mClient.newCall(request).enqueue(response);
            Vlog.d("Get:" + fullURL);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void get(Context pContext, String url, HashMap<String, String> params, Callback response, boolean isShowLoading) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);

        if (CommonFunc.isConnectingToInternet(pContext)) {
            if (isShowLoading)
                CommonFunc.showLoadingView(pContext);
            StringBuilder urlParams = new StringBuilder(url + "?");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                urlParams.append(key).append("=").append(value).append("&");
            }
            String fullURL = urlParams.toString();
            if (fullURL.length() > 0) {
                fullURL = fullURL.substring(0, fullURL.length() - 1);
            }
            request = new Request.Builder()
                    .url(fullURL)
                    .method(METHOD_GET, null)
                    .addHeader("Accept", "application/json")
                    .get()
                    .build();
            mClient.newCall(request).enqueue(response);
            Vlog.d("Get:" + fullURL);
        } else {
        }
    }

    /**
     * DucNT: Hàm này truyền vào params là JsonObject thì sẽ tốt hơn
     * Nhưng truyền vào kiểu Hashmap để đồng bộ với phương thức get
     * thuận tiện cho việc thay đổi phương thức
     */
    public static void post(Context pContext, String url, HashMap<String, String> params, DataResponse response) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        JSONObject objParams = new JSONObject(params);
        RequestBody requestBody = RequestBody.create(JSON, objParams.toString());
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
        Vlog.d("Post:" + url + "?" + params.toString());
    }

    public static void post(Context pContext, String url, HashMap<String, String> params, DataResponse response, boolean isShowLoadingView) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        JSONObject objParams = new JSONObject(params);
        RequestBody requestBody = RequestBody.create(JSON, objParams.toString());
        if (CommonFunc.isConnectingToInternet(pContext)) {
            if (isShowLoadingView)
                CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
        Vlog.d("Post:" + url + "?" + params.toString());
    }

    /**
     * DucNT: Hàm này chỉ nên sử dụng khi dùng multipartform
     * Dev tự add và build request body
     */
    public static void post(final Context pContext, String url, RequestBody params, DataResponse response, boolean hasToken) {
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            Request.Builder builder = new Request.Builder().url(url).post(params);
            builder.header("Accept", "application/json");
            request = builder.build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void put(Context pContext, String url, RequestBody params, DataResponse response) {
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url).put(params)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void put(Context pContext, String url, HashMap<String, String> params, DataResponse response) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        JSONObject objParams = new JSONObject(params);
        RequestBody requestBody = RequestBody.create(JSON, objParams.toString());
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url).put(requestBody)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void delete(Context pContext, String url, RequestBody params, DataResponse response) {
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url)
                    .delete(params)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void delete(Context pContext, String url, HashMap<String, String> params, DataResponse response) {
        addAccessToken(pContext, params);
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        JSONObject objParams = new JSONObject(params);
        RequestBody requestBody = RequestBody.create(JSON, objParams.toString());
        if (CommonFunc.isConnectingToInternet(pContext)) {
            CommonFunc.showLoadingView(pContext);
            request = new Request.Builder()
                    .url(url).delete(requestBody)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(response);
        } else {
            CommonFunc.showDialogOneButton(pContext, pContext.getString(R.string.title_error), pContext.getString(R.string.no_network));
        }
    }

    public static void addAccessToken(Context context, HashMap<String, String> params) {
        params.put(ApiKey.OS, "1");
        ObjLogin objLogin = SharePreferences.getUserInfo(context);
        if (objLogin == null) return;
        if (!TextUtils.isEmpty(objLogin.getAccept_token())) {
            params.put(ApiKey.ACCEPT_TOKEN, objLogin.getAccept_token());
        }
    }

    public static void sendDeviceInfo(Context context, String type, String content, String coin) {
        HashMap<String, String> params = new HashMap<>();
        addDeviceInfoParams(context,params,type,content);
        //Access log coin
        params.put(ApiKey.ACCESS_LOG_COIN, coin);
        //call APi
        Vlog.d("Post:" +  ApiUrl.LOG + "?" + params.toString());
        mClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setReadTimeout(TIME_OUT, TimeUnit.SECONDS);
        mClient.setWriteTimeout(TIME_OUT, TimeUnit.SECONDS);
        JSONObject objParams = new JSONObject(params);
        RequestBody requestBody = RequestBody.create(JSON, objParams.toString());
        if (CommonFunc.isConnectingToInternet(context)) {
            request = new Request.Builder()
                    .url( ApiUrl.LOG)
                    .post(requestBody)
                    .addHeader("Accept", "application/json")
                    .build();
            mClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {

                }
            });
        } else {
            CommonFunc.showDialogOneButton(context, context.getString(R.string.title_error), context.getString(R.string.no_network));
        }
    }

    public static void addDeviceInfoParams(Context context, HashMap<String, String> params, String type, String content){
        String version = Build.VERSION.RELEASE;
        String userId ="";
        ObjLogin objLogin = SharePreferences.getUserInfo(context);
        if(objLogin!=null) userId =objLogin.getPublisher_user_id();
        //User Id
        params.put(ApiKey.PUBLISHER_USER_ID,userId);
        //Thiet bi
        params.put(ApiKey.DEVICE_MODEL, Build.MODEL);
        //Nha Mang
        params.put(ApiKey.NETWORK_PROVIDER, CommonFunc.getNetworkInfomation(context));
        //ip
        params.put(ApiKey.DEVICE_IP, LoginActivity.getIPAddress(true));
        //imei
        params.put(ApiKey.DEVICE_IMEI, CommonFunc.getImei(context));
        //MAC
        params.put(ApiKey.DEVICE_MAC_ADDRESS, LoginActivity.getMACAddress("wlan0"));
        //isWifiEnable
        params.put(ApiKey.IS_WIFI_ENABLED, CommonFunc.isWifiEnable(context) + "");
        //OS
        params.put(ApiKey.OS, "1");
        //OS version
        params.put(ApiKey.DEVICE_VERSION, version);
        //Tui3gang version
        params.put(ApiKey.APP_VERSION, BuildConfig.VERSION_NAME);
        //Thao tac thuc hien
        params.put(ApiKey.ACTION, type);
        //Noi dung
        params.put(ApiKey.CONTENT, content);
        //seri
        params.put(ApiKey.DEVICE_SERI, Build.SERIAL);
        //manufactory
        params.put(ApiKey.DEVICE_MANUFACTORY, Build.MANUFACTURER);

    }
}
