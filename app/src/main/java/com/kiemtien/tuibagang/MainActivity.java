package com.kiemtien.tuibagang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.kiemtien.tuibagang.gcm.GcmIntentService;
import com.kiemtien.tuibagang.login.ObjLogin;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;
import com.kiemtien.tuibagang.mabu.utils.RootUtil;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.utils.SoftKeyboard;
import com.kiemtien.tuibagang.mabu.utils.Temperature;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;
import com.kiemtien.tuibagang.subscreens.canhan.CaNhanFragment;
import com.kiemtien.tuibagang.subscreens.canhan.lienhe.LienHeFragment;
import com.kiemtien.tuibagang.subscreens.canhan.lsnhanthuong.LsNhanThuongFragment;
import com.kiemtien.tuibagang.subscreens.doithuong.DoiThuongFragment;
import com.kiemtien.tuibagang.subscreens.events.EventsFragment;
import com.kiemtien.tuibagang.subscreens.kiemtien.KiemTienFragment;
import com.kiemtien.tuibagang.subscreens.thongbao.ThongBaoFragment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    public static final String NOTIFICATION_COUNT = "notification_count";
    private LinearLayout mTabKiemTien, mTabEvents, mTabDoiThuong, mTabThongBao, mTabCaNhan;
    private Fragment mCurrentFragment;
    private ImageView mToolbarBack;
    private TextView mToolbarTitle, mToolbarCoin;
    private TextView mBadgeThongBao;
    public LinearLayout mBottomLayout;
    public Toolbar mToolbar;
    private String mBatteryTechnology;
    private ObjLogin mObjLogin;
    private int mCpuTemp;
    private BroadcastReceiver mUpdateNotificationCount = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String count = intent.getExtras().getString(NOTIFICATION_COUNT);
            setBadge(count);
        }
    };

    private BroadcastReceiver mBatteryInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mBatteryTechnology = intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
            checkBlueStack();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        initView();
        handleEvent();
        setNavSelected(0);
        mObjLogin = SharePreferences.getUserInfo(this);
        registerReceiver(mUpdateNotificationCount, new IntentFilter(NOTIFICATION_COUNT));
        registerReceiver(mBatteryInfo, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        PermissionUtil.checkPermissionReadPhoneState(this);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUpdateNotificationCount);
        unregisterReceiver(mBatteryInfo);
    }

    private boolean checkBlueStack() {
        String networkProvider = CommonFunc.getNetworkInfomation(MainActivity.this);
        String serial = Build.SERIAL;
        mCpuTemp = Temperature.getTemp();
        boolean isVirtualDevice = serial.contains("unknown") || TextUtils.isEmpty(mBatteryTechnology) || (mCpuTemp == 0 && TextUtils.isEmpty(networkProvider));
        StringBuilder reasonBlock = new StringBuilder();
        if (serial.contains("unknown")) reasonBlock.append("Serial không xác định").append(",");
        if (TextUtils.isEmpty(mBatteryTechnology))
            reasonBlock.append("Công nghệ pin không xác định").append(",");
        if (mCpuTemp == 0) reasonBlock.append("Không xác định được nhiệt độ máy").append(".");
        if(TextUtils.isEmpty(networkProvider)) reasonBlock.append("Không có thông tin nhà mạng").append(".");
        if (isVirtualDevice) {
            HashMap<String, String> params = new HashMap<>();
            params.put(ApiKey.PUBLISHER_USER_ID, mObjLogin.getPublisher_user_id());
            BaseRestClient.addDeviceInfoParams(MainActivity.this, params, "Khoá tài khoản", reasonBlock.toString());
            BaseRestClient.get(MainActivity.this, ApiUrl.URL_BLOCK, params, new DataResponse(MainActivity.this));
            CommonFunc.showDialogOneButton(this, "Cảnh báo!", getResources().getString(R.string.vm_warning), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        settingNav();
        getIntent().removeExtra(GcmIntentService.MESSAGE_TYPE);
        checkDeviceRoot(MainActivity.this, mObjLogin);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    public static void checkDeviceRoot(Context context, ObjLogin obj) {
        if (RootUtil.isDeviceRooted()) {
            if (obj != null) {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiKey.PUBLISHER_USER_ID, obj.getPublisher_user_id());
                BaseRestClient.addDeviceInfoParams(context, params, "Khoá tài khoản", "Sử dụng máy Root");
                BaseRestClient.get(context, ApiUrl.URL_BLOCK, params, new DataResponse(context));
            }
            CommonFunc.showDialogOneButton(context, "Cảnh báo", "Ứng dụng này không được sử dụng trên thiết bị đã root", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });
        }
    }


    private void settingNav() {
        int type = getIntent().getIntExtra(GcmIntentService.MESSAGE_TYPE, -1);
        switch (type) {
            case 0:
                //Default
                setNavSelected(0);
                break;
            case 1:
                //Tab thong bao
                setNavSelected(3);
                break;
            case 2:
                //Tab events
                setNavSelected(1);
                break;
            case 3:
                //Tab Kiem tien
                setNavSelected(0);
                break;
            case 4:
                //Tab doi thuong
                setNavSelected(4);
                goToFragmentClearBackStack(LsNhanThuongFragment.class, new Bundle(), false);
                break;
            case 5:
                //Man hinh cau hoi
                setNavSelected(4);
                goToFragmentClearBackStack(LienHeFragment.class, new Bundle(), false);
                break;
        }
    }


    private void initView() {
        mBottomLayout = (LinearLayout) findViewById(R.id.tabbar_bottom);
        mToolbarBack = (ImageView) findViewById(R.id.toolbar_back);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarCoin = (TextView) findViewById(R.id.toolbar_money);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTabKiemTien = (LinearLayout) findViewById(R.id.nav_tab_kiemtien);
        mTabEvents = (LinearLayout) findViewById(R.id.nav_tab_events);
        mTabDoiThuong = (LinearLayout) findViewById(R.id.nav_tab_doithuong);
        mTabThongBao = (LinearLayout) findViewById(R.id.nav_tab_thongbao);
        mTabCaNhan = (LinearLayout) findViewById(R.id.nav_tab_canhan);
        mBadgeThongBao = (TextView) findViewById(R.id.badge_thongbao);
    }

    public void setBadge(String input) {
        if (TextUtils.isEmpty(input) || input.equalsIgnoreCase("0"))
            mBadgeThongBao.setVisibility(View.GONE);
        else {
            mBadgeThongBao.setVisibility(View.VISIBLE);
            mBadgeThongBao.setText(input);
        }
    }

    public void setToolbarTitle(String title) {
        try {
            if (TextUtils.isEmpty(title)) {
                mToolbarBack.setImageDrawable(getResources().getDrawable(R.drawable.ico_tuibagang_header));
                mToolbarTitle.setText(getResources().getString(R.string.app_name));
                mToolbarCoin.setVisibility(View.VISIBLE);
                mToolbarBack.setEnabled(false);
            } else {
                mToolbarBack.setImageDrawable(getResources().getDrawable(R.drawable.ico_back));
                mToolbarTitle.setText(title);
                mToolbarCoin.setVisibility(View.GONE);
                mToolbarBack.setEnabled(true);
            }
        } catch (Exception e) {
        }
    }


    public void setToolbarMoney(String money) {
        ObjLogin mObjLogin = SharePreferences.getUserInfo(MainActivity.this);
        try {
            int coin = Integer.parseInt(money);
            if (mObjLogin != null)
                mObjLogin.setCoin(coin);
            Gson gson = new Gson();
            String jsonObject = gson.toJson(mObjLogin, ObjLogin.class);
            SharePreferences.saveStringPreference(MainActivity.this, SharePreferences.KEY_PREF_USER_INFO, jsonObject);
        } catch (Exception e) {
            Log.e("DucNT", " set Toolbar money failed");
        }
        mToolbarCoin.setText(convertCoin(money));
    }

    private void handleEvent() {
        mTabKiemTien.setOnClickListener(clickTabNav(0));
        mTabEvents.setOnClickListener(clickTabNav(1));
        mTabDoiThuong.setOnClickListener(clickTabNav(2));
        mTabThongBao.setOnClickListener(clickTabNav(3));
        mTabCaNhan.setOnClickListener(clickTabNav(4));
        mToolbarBack.setOnClickListener(clickBackBtn());
    }

    private View.OnClickListener clickBackBtn() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToolbarTitle("");
                onBackPressed();
            }
        };
    }

    private View.OnClickListener clickTabNav(final int pos) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavSelected(pos);
                SoftKeyboard.hideKeyboard(MainActivity.this);
                clearAllStack();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.tabbar_detail, mCurrentFragment)
                        .commit();
            }
        };
    }

    public void pushFragment(final Fragment fragment, final boolean clearStack) {
        mCurrentFragment = fragment;
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (clearStack) {
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                    R.anim.push_right_in, R.anim.push_right_out);
        }
        transaction.replace(R.id.tabbar_detail, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();

    }


    public void goToFragmentClearBackStack(Class<?> toFragment, Bundle extra, boolean clearStack) {
        try {
            Constructor<?> cons = toFragment.getConstructor();
            Object newFragment = cons.newInstance();

            if (newFragment instanceof Fragment) {
                ((Fragment) newFragment).setArguments(extra);
                pushFragment((Fragment) newFragment, clearStack);
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void setNavSelected(int pos) {
        switch (pos) {
            case 0:
                mTabKiemTien.setSelected(true);
                mTabEvents.setSelected(false);
                mTabDoiThuong.setSelected(false);
                mTabThongBao.setSelected(false);
                mTabCaNhan.setSelected(false);
                mCurrentFragment = new KiemTienFragment();
                mToolbarBack.setEnabled(false);
                break;
            case 1:
                mTabKiemTien.setSelected(false);
                mTabEvents.setSelected(true);
                mTabDoiThuong.setSelected(false);
                mTabThongBao.setSelected(false);
                mTabCaNhan.setSelected(false);
                mCurrentFragment = new EventsFragment();
                break;
            case 2:
                mTabKiemTien.setSelected(false);
                mTabEvents.setSelected(false);
                mTabDoiThuong.setSelected(true);
                mTabThongBao.setSelected(false);
                mTabCaNhan.setSelected(false);
                mCurrentFragment = new DoiThuongFragment();
                break;
            case 3:
                mTabKiemTien.setSelected(false);
                mTabEvents.setSelected(false);
                mTabDoiThuong.setSelected(false);
                mTabThongBao.setSelected(true);
                mTabCaNhan.setSelected(false);
                mCurrentFragment = new ThongBaoFragment();
                break;
            case 4:
                mTabKiemTien.setSelected(false);
                mTabEvents.setSelected(false);
                mTabDoiThuong.setSelected(false);
                mTabThongBao.setSelected(false);
                mTabCaNhan.setSelected(true);
                mCurrentFragment = new CaNhanFragment();
                break;
        }
        SoftKeyboard.hideKeyboard(MainActivity.this);
        clearAllStack();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.tabbar_detail, mCurrentFragment)
                .commit();
    }

    public void clearAllStack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public static String convertCoin(String coin) {
        DecimalFormat df = new DecimalFormat("###.#");
        try {
            float fCoin = Float.parseFloat(coin);
            if (fCoin > 1000000) coin = df.format(fCoin / 1000000) + "M";
            else if (fCoin > 1000) coin = df.format(fCoin / 1000) + "K";
            return coin;
        } catch (Exception e) {
            return coin;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(mCurrentFragment instanceof EventsFragment) mCurrentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
