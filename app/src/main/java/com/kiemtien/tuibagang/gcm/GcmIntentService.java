package com.kiemtien.tuibagang.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;


public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    public static final String MESSAGE_TYPE = "message_type";
    private NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                String msg = extras.getString("message");
                String title = extras.getString("title");
                String type = extras.getString("type");
                int intType = Integer.parseInt(type);
                sendNotification(title,msg, intType);
                String notification_count = extras.getString("notification_count");
                Intent i = new Intent(MainActivity.NOTIFICATION_COUNT);
                i.putExtra(MainActivity.NOTIFICATION_COUNT,notification_count);
                getApplicationContext().sendBroadcast(i);
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void sendNotification(String title,String msg, int type) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(MESSAGE_TYPE, type);
        setupNotification(intent,title, msg);
    }

    private void setupNotification(Intent intent,String title,  String msg) {
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.icon_app);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent resultIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg).setLargeIcon(bm).setSmallIcon(R.drawable.nav_tab1_active);
        mBuilder.setContentIntent(resultIntent);
        mBuilder.setAutoCancel(true);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(uri);
        mNotificationManager.notify(GcmIntentService.NOTIFICATION_ID, mBuilder.build());
    }
}
