package com.kiemtien.tuibagang.login;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.DateUtilities;
import com.kiemtien.tuibagang.mabu.utils.EmailValidator;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.mabu.widget.ImageViewLoading;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.kiemtien.tuibagang.R;

/**
 * Created by DucNT on 4/28/16.
 */
public class ConfirmFragment extends Fragment {
    private ImageViewLoading mAvatar;
    private EditText mUserName, mEmail;
    private TextView mDateOfBirth;
    private TextView mProvince;
    private TextView mBack, mNext;
    private RelativeLayout mProvinceLayout;
    private String mFacebookResponse;
    //Datetime picker
    private DatePickerDialog mPickerDialog;
    private SimpleDateFormat mDateFormatter;
    private AlertDialog.Builder mDialogCity;
    private Activity mActivity;

    private int mGender;
    private String mSocialId;
    private int mCityCode = 0;
    private String mAvatarUrl="";
    private HashMap<String,String> mLoginParams;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        Bundle bundle = getArguments();
        mFacebookResponse = bundle.getString(LoginActivity.FACEBOOK_RESPONSE);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.xacnhan_fragment, container, false);
        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollview);
        mAvatar = (ImageViewLoading) view.findViewById(R.id.avatar);
        mUserName = (EditText) view.findViewById(R.id.ten_taikhoan);
        mEmail = (EditText) view.findViewById(R.id.email);
        mDateOfBirth = (TextView) view.findViewById(R.id.ngaysinh);
        mProvince = (TextView) view.findViewById(R.id.thanhpho);
        mProvinceLayout = (RelativeLayout) view.findViewById(R.id.select_province);
        mBack = (TextView) view.findViewById(R.id.huy_btn);
        mNext = (TextView) view.findViewById(R.id.next_btn);
        initDatePicker();
        initPopupCityPicker();
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
        fillData();
        handleEvent();

        return view;
    }

    private void handleEvent() {
        mDateOfBirth.setOnClickListener(datePicker());
        mProvinceLayout.setOnClickListener(provincePicker());
        mNext.setOnClickListener(onClickNext());
        mBack.setOnClickListener(onClickBack());
    }

    private View.OnClickListener onClickBack() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        };
    }

    private View.OnClickListener onClickNext() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiLogin();
            }
        };
    }

    private View.OnClickListener provincePicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogCity.show();
            }
        };
    }

    private View.OnClickListener datePicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPickerDialog.show();
            }
        };
    }

    private void initPopupCityPicker() {
        final String[] arrCity = getResources().getStringArray(R.array.arr_citys);
        mDialogCity = new AlertDialog.Builder(mActivity);
        mDialogCity.setItems(arrCity, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mProvince.setText(arrCity[which]);
                dialog.dismiss();
                mCityCode = which + 1;
            }
        });

    }


    private void initDatePicker() {
        Calendar newCalendar = Calendar.getInstance();
        int iDefaultYear, iDefaultMonth, iDefaultDay;
        iDefaultYear = newCalendar.get(Calendar.YEAR) - 20;

        iDefaultMonth = newCalendar.get(Calendar.MONTH);
        iDefaultDay = newCalendar.get(Calendar.DAY_OF_MONTH);
        mPickerDialog = new DatePickerDialog(mActivity, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mDateFormatter = new SimpleDateFormat(DateUtilities.DATE_FORMAT_DDMMYYYY, Locale.US);
                mDateOfBirth.setText(mDateFormatter.format(newDate.getTime()));
                mDateOfBirth.setTextColor(mActivity.getResources().getColor(R.color.text_selected));
            }
        }, iDefaultYear, iDefaultMonth, iDefaultDay);
        mPickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        mPickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void fillData() {
        String email = "", name = "", birthday = "", gender = "";
        try {
            JSONObject jsonObject = new JSONObject(mFacebookResponse);
            mSocialId = jsonObject.getString("id");
            email = jsonObject.getString("email");
            mAvatarUrl = "http://graph.facebook.com/" + mSocialId + "/picture?type=large";
            name = jsonObject.getString("name");
            gender = jsonObject.getString("gender");
            if (!gender.equalsIgnoreCase("female")) mGender = 1;
            birthday = jsonObject.getString("birthday");
            if (birthday.length() > 0) {
                birthday = DateUtilities.convertDate(birthday, DateUtilities.DATE_FORMAT_MMDDYYYY, DateUtilities.DATE_FORMAT_DDMMYYYY);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAvatar.setCircleImg(mAvatarUrl, mActivity);
        if (!TextUtils.isEmpty(email)) {
            mEmail.setHint(email);
        }

        if (!TextUtils.isEmpty(name)) {
            mUserName.setHint(name);
        }

        if (!TextUtils.isEmpty(birthday)) {
            mDateOfBirth.setText(birthday);
            mDateOfBirth.setTextColor(getResources().getColor(R.color.text_hint_default));
        }
    }


    private void callApiLogin() {
        if (!checkPermissionReadPhoneState(mActivity)) {
            return;
        }

        String email = "", name = "", birthday = "";
        String province = mProvince.getText().toString();
        String gcmId = SharePreferences.getDeviceTokenPreference(mActivity, SharePreferences.KEY_PREF_REGISTER_GCM);

        email = mEmail.getText().toString();
        if (TextUtils.isEmpty(email)) email = mEmail.getHint().toString();

        name = mUserName.getText().toString();
        if (TextUtils.isEmpty(name)) name = mUserName.getHint().toString();

        birthday = mDateOfBirth.getText().toString();
        if (TextUtils.isEmpty(birthday)) birthday = mDateOfBirth.getText().toString();

        //DucNT: defaul la Ha Noi- 17
        if (mCityCode == 0) mCityCode = 17;
        EmailValidator emailValidator = new EmailValidator();
        boolean trueEmail = emailValidator.validate(email);
        StringBuilder error = new StringBuilder();
        if (TextUtils.isEmpty(name)||name.equalsIgnoreCase(getResources().getString(R.string.taikhoan_hint)))
            error.append(getResources().getString(R.string.user_name_must_not_null)).append("\n");
        if (TextUtils.isEmpty(email)||email.equalsIgnoreCase(getResources().getString(R.string.email_hint)))
            error.append(getResources().getString(R.string.email_invalid)).append("\n");
        if (!trueEmail) error.append(getResources().getString(R.string.email_invalid)).append("\n");
        if (TextUtils.isEmpty(birthday)||birthday.equalsIgnoreCase(getResources().getString(R.string.birthday_hint)))
            error.append(getResources().getString(R.string.birthday_not_null)).append("\n");
        if (TextUtils.isEmpty(province))
            error.append(getResources().getString(R.string.province_not_null)).append("\n");
        if (!TextUtils.isEmpty(error.toString())) {
            CommonFunc.showDialogOneButton(mActivity, getResources().getString(R.string.title_error), error.toString());
            return;
        }
        String manufactory = android.os.Build.MANUFACTURER;
        String deviceModel = Build.MODEL;
        birthday = DateUtilities.convertDate(birthday, DateUtilities.DATE_FORMAT_DDMMYYYY, DateUtilities.DATE_FORMAT_YYYYMMDD);
        mLoginParams = new HashMap<>();
        mLoginParams.put(ApiKey.PUBLISHER_SOCIAL_ID, mSocialId);
        mLoginParams.put(ApiKey.PUBLISHER_EMAIL, email);
        mLoginParams.put(ApiKey.PUBLISHER_AVATAR_URL, mAvatarUrl);
        mLoginParams.put(ApiKey.PUBLISHER_FACEBOOK_NAME, name);
        mLoginParams.put(ApiKey.BIRTHDAY, birthday);
        mLoginParams.put(ApiKey.PUBLISHER_SEX, mGender + "");//DucNT: 1 la male
        mLoginParams.put(ApiKey.DEVICE_ID, CommonFunc.getUuid(mActivity));
        mLoginParams.put(ApiKey.DEVICE_MODEL, deviceModel);
        mLoginParams.put(ApiKey.DEVICE_MANUFACTORY, manufactory);
        mLoginParams.put(ApiKey.PUBLISHER_CITY, mCityCode + "");
        mLoginParams.put(ApiKey.GCM_ID, gcmId);
        addLoginParams();
        BaseRestClient.post(mActivity, ApiUrl.URL_LOGIN, mLoginParams, new DataResponse(mActivity) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject objResponse = new JSONObject(response);
                    JSONObject data = objResponse.getJSONObject("data");
                    SharePreferences.saveStringPreference(mActivity, SharePreferences.KEY_PREF_USER_INFO, data.toString());
                    BaseRestClient.sendDeviceInfo(getActivity(),"Đăng nhập","Đăng nhập","0");//TODO
                    Intent i = new Intent(mActivity, MainActivity.class);
                    startActivity(i);
                    mActivity.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public  void addLoginParams(){
        if(!PermissionUtil.checkPermissionWifiState(getActivity())){
            return;
        }
        String version = Build.VERSION.RELEASE;
        mLoginParams.put(ApiKey.DEVICE_VERSION, version);
        mLoginParams.put(ApiKey.DEVICE_IP, LoginActivity.getIPAddress(true));
        mLoginParams.put(ApiKey.DEVICE_MAC_ADDRESS, LoginActivity.getMACAddress("wlan0"));
        mLoginParams.put(ApiKey.DEVICE_SERI, Build.SERIAL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtil.MY_PERMISSION_READ_PHONE) callApiLogin();
    }

    public  boolean checkPermissionReadPhoneState(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    PermissionUtil.MY_PERMISSION_READ_PHONE);
            return false;
        }
        return true;
    }
}
