package com.kiemtien.tuibagang.login;

/**
 * Created by DucNT on 4/20/16.
 */
public class ObjLogin implements Comparable {

    private String publisher_user_id;
    private String publisher_social_id;
    private String publisher_email;
    private String publisher_avatar_url;
    private String publisher_facebook_name;
    private int publisher_age;
    private String publisher_city;
    private String publisher_sex;
    private int coin;
    private int notification_count;
    private String share_code;
    private String accept_token;
    private String share_code_status;

    public String getShare_code_status() {
        return share_code_status;
    }

    public void setShare_code_status(String share_code_status) {
        this.share_code_status = share_code_status;
    }

    public String getPublisher_user_id() {
        return publisher_user_id;
    }

    public void setPublisher_user_id(String publisher_user_id) {
        this.publisher_user_id = publisher_user_id;
    }

    public String getPublisher_social_id() {
        return publisher_social_id;
    }

    public void setPublisher_social_id(String publisher_social_id) {
        this.publisher_social_id = publisher_social_id;
    }

    public String getPublisher_email() {
        return publisher_email;
    }

    public void setPublisher_email(String publisher_email) {
        this.publisher_email = publisher_email;
    }

    public String getPublisher_avatar_url() {
        return publisher_avatar_url;
    }

    public void setPublisher_avatar_url(String publisher_avatar_url) {
        this.publisher_avatar_url = publisher_avatar_url;
    }

    public String getPublisher_facebook_name() {
        return publisher_facebook_name;
    }

    public void setPublisher_facebook_name(String publisher_facebook_name) {
        this.publisher_facebook_name = publisher_facebook_name;
    }

    public int getPublisher_age() {
        return publisher_age;
    }

    public void setPublisher_age(int publisher_age) {
        this.publisher_age = publisher_age;
    }

    public String getPublisher_city() {
        return publisher_city;
    }

    public void setPublisher_city(String publisher_city) {
        this.publisher_city = publisher_city;
    }

    public String getPublisher_sex() {
        return publisher_sex;
    }

    public void setPublisher_sex(String publisher_sex) {
        this.publisher_sex = publisher_sex;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getNotification_count() {
        return notification_count;
    }

    public void setNotification_count(int notification_count) {
        this.notification_count = notification_count;
    }

    public String getShare_code() {
        return share_code;
    }

    public void setShare_code(String share_code) {
        this.share_code = share_code;
    }

    public String getAccept_token() {
        return accept_token;
    }

    public void setAccept_token(String accept_token) {
        this.accept_token = accept_token;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }
}
