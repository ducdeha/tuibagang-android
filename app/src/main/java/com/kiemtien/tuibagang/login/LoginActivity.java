package com.kiemtien.tuibagang.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.kiemtien.tuibagang.BuildConfig;
import com.kiemtien.tuibagang.MainActivity;
import com.kiemtien.tuibagang.R;
import com.kiemtien.tuibagang.mabu.base.BaseActivity;
import com.kiemtien.tuibagang.mabu.utils.CommonFunc;
import com.kiemtien.tuibagang.mabu.utils.OnInternetButtonClickListener;
import com.kiemtien.tuibagang.mabu.utils.PermissionUtil;
import com.kiemtien.tuibagang.mabu.utils.RootUtil;
import com.kiemtien.tuibagang.mabu.utils.SharePreferences;
import com.kiemtien.tuibagang.network.ApiKey;
import com.kiemtien.tuibagang.network.ApiUrl;
import com.kiemtien.tuibagang.network.BaseRestClient;
import com.kiemtien.tuibagang.network.DataResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by DucNT on 4/20/16.
 */
public class LoginActivity extends BaseActivity {
    public static final String FACEBOOK_RESPONSE = "facebook_response";
    private TextView mLoginFacebook;
    private LoginButton mFacebookBtn;
    private CallbackManager mCallbackManager;
    private JSONObject mObjFacebookResponse;
    private HashMap<String, String> mLoginParams;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.login_activity);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(LoginActivity.this);
        MainActivity.checkDeviceRoot(LoginActivity.this,null);
    }


    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(LoginActivity.this);
    }

    private void initView() {
        mLoginFacebook = (TextView) findViewById(R.id.login_btn);
        mFacebookBtn = (LoginButton) findViewById(R.id.loginbutton);
        configFacebook();
        mLoginFacebook.setOnClickListener(new OnInternetButtonClickListener(LoginActivity.this) {
            @Override
            public void onButtonClicked(View v) {
                LoginManager.getInstance().logOut();
                mFacebookBtn.performClick();
            }
        });
    }

    private void configFacebook() {
        mFacebookBtn.setReadPermissions(Arrays.asList("email, user_birthday, user_friends, user_photos"));
        mCallbackManager = CallbackManager.Factory.create();
        mFacebookBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Gson gson = new Gson();
                String strAccesstoken = gson.toJson(loginResult.getAccessToken());
                SharePreferences.saveStringPreference(LoginActivity.this, SharePreferences.KEY_PREF_FACEBOOK_ACCESS_TOKEN, strAccesstoken);
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                mObjFacebookResponse = object;
                                callApiLogin();
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }

    private void gotoConfirmFragment(String response) {
        Bundle bundle = new Bundle();
        bundle.putString(FACEBOOK_RESPONSE, response);
        ConfirmFragment confirmFragment = new ConfirmFragment();
        confirmFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                R.anim.push_right_in, R.anim.push_right_out);
        transaction.replace(R.id.login_activity, confirmFragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();

    }

    private void callApiLogin() {
        String socialId = "";
        try {
            socialId = mObjFacebookResponse.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!PermissionUtil.checkPermissionReadPhoneState(LoginActivity.this)) {
            return;
        }
        String gcmId = SharePreferences.getDeviceTokenPreference(this, SharePreferences.KEY_PREF_REGISTER_GCM);
        String manufactory = android.os.Build.MANUFACTURER;
        String deviceModel = Build.MODEL;
        mLoginParams = new HashMap<>();
        mLoginParams.put(ApiKey.PUBLISHER_SOCIAL_ID, socialId);
        mLoginParams.put(ApiKey.DEVICE_ID, CommonFunc.getUuid(this));
        mLoginParams.put(ApiKey.DEVICE_MODEL, deviceModel);
        mLoginParams.put(ApiKey.DEVICE_MANUFACTORY, manufactory);
        mLoginParams.put(ApiKey.GCM_ID, gcmId);
        addLoginParams();
        BaseRestClient.post(LoginActivity.this, ApiUrl.URL_LOGIN, mLoginParams, new DataResponse(LoginActivity.this) {
            @Override
            public void onRealSuccess(String response) {
                super.onRealSuccess(response);
                try {
                    JSONObject objResponse = new JSONObject(response);
                    JSONObject data = objResponse.getJSONObject("data");
                    SharePreferences.saveStringPreference(LoginActivity.this, SharePreferences.KEY_PREF_USER_INFO, data.toString());
                    BaseRestClient.sendDeviceInfo(LoginActivity.this, "Đăng nhập", "Đăng nhập", "0");
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void on404() {
                super.on404();
                gotoConfirmFragment(mObjFacebookResponse.toString());
            }
        });
    }

    public void addLoginParams() {
        if (!PermissionUtil.checkPermissionWifiState(LoginActivity.this)) {
            return;
        }
        String version = Build.VERSION.RELEASE;
        mLoginParams.put(ApiKey.DEVICE_VERSION, version);
        mLoginParams.put(ApiKey.DEVICE_IP, getIPAddress(true));
        mLoginParams.put(ApiKey.DEVICE_MAC_ADDRESS, getMACAddress("wlan0"));
        mLoginParams.put(ApiKey.DEVICE_SERI, Build.SERIAL);
    }

    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";

    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionUtil.MY_PERMISSION_READ_PHONE) callApiLogin();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
